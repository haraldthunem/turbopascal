program View;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� INFO 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{� File    : VIEW.PAS                                                       �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : View text files.                                               �}
{� Updated : September 2 1993                                               �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$A+   Word align data                                                       }
{$B-   Short-circuit Boolean expression evaluation                           }
{$E-   Disable linking with 8087-emulating run-time library                  }
{$G+   Enable 80286 code generation                                          }
{$R-   Disable generation of range-checking code                             }
{$S-   Disable generation of stack-overflow checking code                    }
{$V-   String variable checking                                              }
{$X-   Disable Turbo Pascal's extended syntax                                }
{$N+   80x87 code generation                                                 }
{$D-   Disable generation of debug information                               }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
uses  Dos,
      UScreen,
      UKeys,
      UStrs;

const MaxLines = 5000;

type  PLine     = ^string;
      TList     = array[1..MaxLines] of PLine;
      PSearchRec= ^SearchRec;

var   List      : TList;
      NumLines  : word;
      SearchKey,
      Filename,
      Dir       : string;
      FileList  : array[1..500] of PSearchRec;
      InitScr   : pointer;
      CursRow,
      CursCol   : byte;

procedure SelectFile(SearchKey: string; var Dir,Filename: string);
const BoxRow   = 2;
      BoxCol   = 25;
      BoxRows  = 22;
      BoxCols  = 26;
      BoxAttr  = White+CyanBG;
      FileAttr = White+CyanBG;
      DirAttr  = LightGreen+CyanBG;
      SelAttr  = White+RedBG;
var   i,Start,
      NumFiles : word;
      TmpCount : byte;
      TmpName  : string;
      Scr      : pointer;

  procedure GetList(var Dir: string);
  var S: SearchRec;
  begin
    NumFiles := 0;

    { Find directories first }
    FindFirst(Dir+'*.*',AnyFile,S);
    while DosError = 0 do
    begin
      if (S.Attr and Directory = Directory) and (S.Name<>'.') then
      begin
        Inc(NumFiles);
        GetMem(FileList[NumFiles],SizeOf(SearchRec));
        FileList[NumFiles]^ := S;
      end;
      FindNext(S);
    end;

    { Then find files }
    FindFirst(Dir+SearchKey,AnyFile,S);
    while DosError = 0 do
    begin
      if (S.Attr and Archive = Archive)
      or (S.Attr and Hidden  = Hidden)
      or (S.Attr and ReadOnly= ReadOnly)
      or (S.Attr and SysFile = SysFile) then
      begin
        Inc(NumFiles);
        GetMem(FileList[NumFiles],SizeOf(SearchRec));
        FileList[NumFiles]^ := S;
      end;
      FindNext(S);
    end;
  end;

  procedure SortFileList;
  var i: integer;
      b: boolean;
      t: PSearchRec;
  begin
    repeat
      b := true;
      for i := 1 to NumFiles-1 do
      if FileList[i]^.Name > FileList[i+1]^.Name then
      begin
        t:=FileList[i]; FileList[i]:=FileList[i+1]; FileList[i+1]:=t; b:=False;
      end;
    until b;
    repeat
      b := true;
      for i := 1 to NumFiles-1 do
      if (FileList[i]^.Attr and Directory<>Directory) and (FileList[i+1]^.Attr and Directory=Directory) then
      begin
        t:=FileList[i]; FileList[i]:=FileList[i+1]; FileList[i+1]:=t; b:=False;
      end;
    until b;
  end;

  procedure DeleteList;
  var i: word;
  begin
    for i := 1 to NumFiles do
      FreeMem(FileList[i],SizeOf(SearchRec));
    NumFiles := 0;
  end;

  procedure WriteFile(Row: byte; Num: word);
  var p: byte;
      TAttr,
      FAttr: byte;
      Name : string;
      Size : longint;
  begin
    Name  := FileList[Num]^.Name;
    Size  := FileList[Num]^.Size;
    FAttr := FileList[Num]^.Attr;
    if FAttr and Directory = Directory then
      TAttr := DirAttr
    else TAttr := FileAttr;
    p := Pos('.',Name);
    if (Name<>'.') and (Name<>'..') and (p>0) then
    while p < 9 do
    begin
      Insert(' ',Name,p);
      Inc(p);
    end;
    Fill(Row,BoxCol+1,1,BoxCols-2,FileAttr,' ');
    WriteStr(Row,BoxCol+2,TAttr,Name);
    if FAttr and Directory = Directory then
      WriteStr(Row,BoxCol+16,TAttr,'')
    else WriteStr(Row,BoxCol+16,TAttr,StrLF(Size,8));
  end;

  procedure ShowGlider(Current,NumFiles: word);
  var Row: byte;
  begin
    Fill(BoxRow+2,BoxCol+BoxCols-1,BoxRows-4,1,White+BlackBG,'�');
    WriteStr(BoxRow+1,BoxCol+BoxCols-1,White+BlackBG,#24);
    WriteStr(BoxRow+BoxRows-2,BoxCol+BoxCols-1,White+BlackBG,#25);
    if NumFiles > 1 then
      Row := BoxRow+2+Trunc((BoxRows-4) * (Current-1)/(NumFiles-1))
    else Row := BoxRow+2;
    if Row > BoxRows-1 then
      Row := BoxRows-1;
    WriteStr(Row,BoxCol+BoxCols-1,White+BlackBG,'�');
  end;

begin
  GetMem(Scr,2*(BoxRows+1)*(BoxCols+2));
  StoreToMem(BoxRow,BoxCol,BoxRows+1,BoxCols+2,Scr^);
  GetList(Dir);
  SortFileList;
  Box(BoxRow,BoxCol,BoxRows,BoxCols,BoxAttr,SingleBorder,' ');
  AddShadow(BoxRow,BoxCol,BoxRows,BoxCols);
  for i := 1 to BoxRows-2 do
  if i <= NumFiles then
    WriteFile(BoxRow+i,i);
  Start := 1;
  i := 1;
  repeat
    FillAt(BoxRow+i,BoxCol+1,1,BoxCols-2,SelAttr);
    ShowGlider(Start+i-1,NumFiles);
    InKey(Ch,Key);
    WriteFile(BoxRow+i,Start+i-1);
    case Key of
      UpArrow  : if i > 1 then Dec(i)
                 else if (Start+i) > 2 then
                 begin
                   ScrollDown(BoxRow+1,BoxCol+1,BoxRows-2,BoxCols-2,BoxAttr);
                   Dec(Start);
                   WriteFile(BoxRow+i,Start+i-1);
                 end;
      DownArrow: if (i < (BoxRows-2)) and (i < NumFiles) then Inc(i)
                 else if (Start+i) < NumFiles then
                 begin
                   ScrollUp(BoxRow+1,BoxCol+1,BoxRows-2,BoxCols-2,BoxAttr);
                   Inc(Start);
                   WriteFile(BoxRow+i,Start+i-1);
                 end;
      PgUp     : if Start > 1 then
                 begin
                   Box(BoxRow,BoxCol,BoxRows,BoxCols,BoxAttr,SingleBorder,' ');
                   if Start > BoxRows-2 then
                     Dec(Start,BoxRows-2)
                   else
                     Start := 1;
                   for TmpCount := 1 to BoxRows-2 do
                   if Start+TmpCount-1 <= NumFiles then
                     WriteFile(BoxRow+TmpCount,Start+TmpCount-1);
                 end;
      PgDn     : if (Start+BoxRows-2) < NumFiles then
                 begin
                   Box(BoxRow,BoxCol,BoxRows,BoxCols,BoxAttr,SingleBorder,' ');
                   if NumFiles-(Start+BoxRows-2) > BoxRows-2 then
                     Inc(Start,BoxRows-2)
                   else
                     Start := NumFiles - (BoxRows-2);
                   for TmpCount := 1 to BoxRows-2 do
                   if Start+TmpCount-1 <= NumFiles then
                     WriteFile(BoxRow+TmpCount,Start+TmpCount-1);
                 end;
      HomeKey  : begin
                   Box(BoxRow,BoxCol,BoxRows,BoxCols,BoxAttr,SingleBorder,' ');
                   for i := 1 to BoxRows-2 do
                   if i <= NumFiles then
                     WriteFile(BoxRow+i,i);
                   Start := 1;
                   i := 1;
                 end;
      EndKey   : begin
                   Box(BoxRow,BoxCol,BoxRows,BoxCols,BoxAttr,SingleBorder,' ');
                   if NumFiles >= BoxRows-2 then
                   begin
                     i := BoxRows-2;
                     Start := NumFiles - (BoxRows-2);
                   end
                   else begin
                     i := NumFiles;
                     Start := 1;
                   end;
                   for TmpCount := 1 to i do
                     WriteFile(BoxRow+TmpCount,Start+TmpCount-1);
                 end;
      Return   : if FileList[Start+i-1]^.Attr and Directory = Directory then
                 begin
                   if FileList[Start+i-1]^.Name = '..' then
                   begin
                     TmpName := Dir;
                     TmpCount := Length(TmpName);
                     Delete(TmpName,TmpCount,1);
                     repeat
                       Dec(TmpCount);
                     until TmpName[TmpCount] = '\';
                     TmpName := Copy(TmpName,1,TmpCount);
                   end
                   else
                     TmpName := Dir+FileList[Start+i-1]^.Name+'\';
                   Key := NullKey;
                   Dir := TmpName;
                   DeleteList;
                   GetList(Dir);
                   SortFileList;
                   Box(BoxRow,BoxCol,BoxRows,BoxCols,BoxAttr,SingleBorder,' ');
                   AddShadow(BoxRow,BoxCol,BoxRows,BoxCols);
                   for i := 1 to BoxRows-2 do
                   if i <= NumFiles then
                     WriteFile(BoxRow+i,i);
                   Start := 1;
                   i := 1;
                 end
                 else
                   Filename := FileList[Start+i-1]^.Name;
    end;
    FillAt(BoxRow+i,BoxCol+1,1,BoxCols-2,SelAttr);
  until Key in [Return,Escape];
  DeleteList;
  StoreToScr(BoxRow,BoxCol,BoxRows+1,BoxCols+2,Scr^);
  FreeMem(Scr,2*(BoxRows+1)*(BoxCols+2));
end;


procedure GetFilename(var Dir,Filename: string);
var Path: PathStr;
    D   : DirStr;
    N   : NameStr;
    Ext : ExtStr;
begin
  if ParamCount = 0 then
  begin
    GetDir(0,Dir);
    if Length(Dir) > 3 then
      Dir := Dir + '\';
    SearchKey := '*.*';
    SelectFile(SearchKey,Dir,Filename);
  end;
  if ParamCount > 0 then
  begin
    Path := UpCaseStr(ParamStr(1));
    FSplit(Path,D,N,Ext);
    Dir := D;
    Filename := N + Ext;
    if Filename = '' then
      Filename := '*.*';
    if (Pos('*',Filename) > 0) or (Pos('?',Filename) > 0) then
    begin
      if Length(Dir) <= 0 then
      begin
        GetDir(0,Dir);
        if Length(Dir) > 3 then
          Dir := Dir + '\';
      end;
      SearchKey := Filename;
      SelectFile(SearchKey,Dir,Filename);
    end;
  end;
end;


procedure ReadFile(Dir,Filename: string);
var f: text;
    s: string;
    p: byte;
begin
  NumLines := 0;
  {$I-}
  Assign(f,Dir+Filename);
  Reset(f);
  if IOResult <> 0 then Exit;
  repeat
    ReadLn(f,s);
    p := Pos(#9,s);
    if p > 0 then
    repeat
      Delete(s,p,1);
      Insert('        ',s,p);
      p := Pos(#9,s);
    until p <= 0;
    Inc(NumLines);
    GetMem(List[NumLines],Length(s)+1);
    List[NumLines]^ := s;
  until Eof(f);
  Close(f);
  {$I+}
end;


procedure Background(StartCol,StartLine: word;  Name: string);
begin
  SetCursor(CursorOff);
  Fill(1,1,1,80,Yellow+CyanBG,' ');
  WriteStr(1,2,Yellow+CyanBG,'Col '+StrLF(StartCol,1)+'  Line '+StrLF(StartLine,1)+' of '+StrLF(NumLines,1));
  Fill(25,1,1,80,Yellow+CyanBG,' ');
  WriteStr(25,80-Length(Name),Yellow+CyanBG,Name);
end;


procedure WriteLine(Row,StartCol,LineNum: word);
var s: string;
begin
  if (LineNum <= NumLines) then
  begin
    s := List[LineNum]^;
    if Length(s) > (StartCol-1) then
      Delete(s,1,StartCol-1)
    else s := '';
    if Length(s) > 80 then
      s := Copy(s,1,80);
    Fill(Row,1,1,80,White+BlueBG,' ');
    WriteStr(Row,1,SameAttr,s);
  end;
end;


procedure WritePage(StartCol,StartLine: word);
var i: byte;
begin
  Fill(2,1,23,80,White+BlueBG,' ');
  for i := 1 to 23 do
    WriteLine(i+1,StartCol,StartLine+i-1);
end;


procedure ViewFile;
var i,StartCol,StartLine: word;
begin
  StartLine := 1;
  StartCol := 1;
  WritePage(StartCol,StartLine);
  repeat
    Background(StartCol,StartLine,Dir+Filename);
    InKey(Ch,Key);
    case Key of
      UpArrow       : if StartLine > 1 then
                      begin
                        Dec(StartLine);
                        ScrollDown(2,1,23,80,White+BlueBG);
                        WriteLine(2,StartCol,StartLine);
                      end;
      DownArrow     : if StartLine < NumLines then
                      begin
                        Inc(StartLine);
                        ScrollUp(2,1,23,80,White+BlueBG);
                        WriteLine(24,StartCol,StartLine+22);
                      end;
      PgDn          : if (StartLine+22) < NumLines then
                      begin
                        Inc(StartLine,23);
                        WritePage(StartCol,StartLine);
                      end;
      PgUp          : begin
                        if StartLine > 23 then
                          Dec(StartLine,23)
                        else StartLine := 1;
                        WritePage(StartCol,StartLine);
                      end;
      HomeKey       : begin
                        StartCol := 1;
                        StartLine := 1;
                        WritePage(StartCol,StartLine);
                      end;
      EndKey        : begin
                        StartCol := 1;
                        StartLine := NumLines - 22;
                        WritePage(StartCol,StartLine);
                      end;
      RightArrow    : if StartCol < 255 then
                      begin
                        Inc(StartCol);
                        WritePage(StartCol,StartLine);
                      end;
      LeftArrow     : if StartCol > 1 then
                      begin
                        Dec(StartCol);
                        WritePage(StartCol,StartLine);
                      end;
      CtrlRightArrow: if StartCol < 246 then
                      begin
                        Inc(StartCol,10);
                        WritePage(StartCol,StartLine);
                      end;
      CtrlLeftArrow : begin
                        if StartCol > 10 then
                          Dec(StartCol,10)
                        else StartCol := 1;
                        WritePage(StartCol,StartLine);
                      end;
      F3            : begin
                        SelectFile(SearchKey,Dir,Filename);
                        if Key = Return then
                        begin
                          ReadFile(Dir,Filename);
                          StartCol := 1;
                          StartLine := 1;
                          WritePage(StartCol,StartLine);
                        end
                        else Key := NullKey;
                      end;
    end;
  until Key = Escape;
end;


begin
  GetMem(InitScr,4000);
  StoreToMem(1,1,25,80,InitScr^);
  CursorPos(CursRow,CursCol);
  SetCursor(CursorOff);
  Fill(1,1,25,80,White+BlueBG,' ');
  GetFilename(Dir,Filename);
  if Key=Escape then
  begin
    StoreToScr(1,1,25,80,InitScr^);
    FreeMem(InitScr,4000);
    GotoRC(CursRow,CursCol);
    WriteLn;
    Halt(1);
  end;
  ReadFile(Dir,Filename);
  ViewFile;
  SetCursor(CursorUnderline);
  StoreToScr(1,1,25,80,InitScr^);
  FreeMem(InitScr,4000);
  GotoRC(CursRow,CursCol);
end.
