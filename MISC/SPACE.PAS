program Space;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� INFO 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{� File    : SPACE.PAS                                                      �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : Object demo program.                                           �}
{� Updated : October 28 1993                                                �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$A+   Word align data                                                       }
{$B-   Short-circuit Boolean expression evaluation                           }
{$E-   Disable linking with 8087-emulating run-time library                  }
{$G+   Enable 80286 code generation                                          }
{$R-   Disable generation of range-checking code                             }
{$S-   Disable generation of stack-overflow checking code                    }
{$V-   String variable checking                                              }
{$X+   Enable Turbo Pascal's extended syntax                                 }
{$N+   80x87 code generation                                                 }
{$D-   Disable generation of debug information                               }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
uses
  UKeys,
  Graph,   { TP graphics routines }
  UGrInit; { HT Graphics routines }

const
  MaxStars= 100;
  Right   = 1;
  Left    = -1;

type
  PStar = ^TStar;
  TStar = object
            OldAttr,Attr: byte;
            OldRadius,
            Angle,Speed,Radius: single;
            constructor Init(A,S,R: single; BAttr: byte);
            procedure DrawStar(Remove: boolean);
            procedure MoveStar;
            procedure CheckStar;
          end;

var
  Stars : array[1..MaxStars] of PStar;
  i     : word;


function Power(x,e: single): single;
begin
  if x > 0.0 then
    Power := Exp(e*Ln(x))
  else Power := 0.0;
end; { Power }


constructor TStar.Init(A,S,R: single; BAttr: byte);
begin
  Attr := BAttr;
  Angle := A;
  Speed := S;
  Radius := R;
  OldRadius := R;
end; { TStar.Init }


procedure TStar.DrawStar(Remove: boolean);
var
  odx,ody,dx,dy: word;
begin
  dx := (MaxX div 2) + Round(Radius * Cos(Angle));
  dy := (MaxY div 2) + Round(Radius * Sin(Angle));
  if Attr=White then
  begin
    odx := (MaxX div 2) + Round(OldRadius * Cos(Angle));
    ody := (MaxY div 2) + Round(OldRadius * Sin(Angle));
    SetWriteMode(XOrPut);
    SetColor(Attr);
    Line(dx,dy,odx,ody);
    SetWriteMode(NormalPut);
    if Remove then
      OldRadius := Radius;
    Exit;
  end;
  if Remove then
    PutPixel(dx,dy,OldAttr)
  else
  begin
    OldAttr := GetPixel(dx,dy);
    if OldAttr in [White,LightGray] then
      OldAttr := Black;
    PutPixel(dx,dy,Attr);
  end;
end; { TStar.DrawStar }


procedure TStar.MoveStar;
begin
  DrawStar(true);
  Radius := Power(Radius,Speed);
  DrawStar(false);
end; { TStar.MoveStar }


procedure TStar.CheckStar;
begin
  if Radius > 450 then
  begin
    Radius := 200*Random+1.0;
    Angle  := 2*pi*Random;
    if Radius < 50 then
    begin
      Speed := 1.010 + 0.005*Random;
      Attr := White;
    end
    else begin
      Speed := 1.000 + 0.001*Random;
      Attr := LightGray;
    end;
    OldRadius := Radius;
    DrawStar(false);
  end;
end; { TStar.CheckStar }


procedure InitStars;
var
  i: word;
  r,a: single;
begin
  for i := 1 to MaxStars do
  begin
    a := 2*pi*Random;
    r := 300*Random+1.0;
    if r < 50 then
      Stars[i] := New(PStar, Init(a,1.010+0.005*Random,r,White))
    else Stars[i] := New(PStar, Init(a,1.000+0.001*Random,r,LightGray));
    Stars[i]^.DrawStar(false);
  end;
end; { InitStars }


procedure DoneStars;
var
  i: word;
begin
  for i := 1 to MaxStars do
    Dispose(Stars[i]);
end; { DoneStars }


procedure Galaxy(X,Y,Radius,Angle,Ratio: single; Direction: shortint);
const
  Rad = 0.1;
var
  pi13,a,a2,b,r,r2: single;
  c,i,j,dx,dy: word;
begin
  pi13 := pi/3.0;
  for i := 1 to Round(10*Radius) do
  begin
    r := 0.0;
    for j := 1 to 4 do
      r := r + 0.8*Radius*(0.5-Random);
    if r < 0 then r := -r;
    a := Angle+pi*(r/Radius);
    for j := 1 to 6 do
    begin
      a2 := a+j*pi13;
      dx := Round(X)+Direction*Round(r*Cos(a2) + Rad*Radius*(0.5-Random));
      dy := Round(Y)+Round(r*Sin(a2) + Rad*Radius*(0.5-Random));
      c := 1+Trunc(9*(Radius-r)/Radius);
      c := c + Round(2*(0.5-Random));
      PutPixel(dx,dy,c);
    end;
  end;
  for i := 1 to Round(50*Radius) do
  begin
    a := 2*pi*Random;
    r := 0.0;
    for j := 1 to 4 do
      r := r + 0.8*Radius*(0.5-Random);
    if r < 0 then r := -r;
    dx := Round(X + r*Cos(a));
    dy := Round(Y + r*Sin(a));
    c := 1+Trunc(9*(Radius-r)/Radius);
    c := c + Round(2*(0.5-Random));
    PutPixel(dx,dy,c);
  end;
end; { Galaxy }


procedure Palette;
var
  i: byte;
begin
  for i := 1 to 14 do
    SetPalette(i,i);
  SetRGBPalette(1,30,5,5);
  SetRGBPalette(2,35,10,10);
  SetRGBPalette(3,40,15,15);
  SetRGBPalette(4,45,20,20);
  SetRGBPalette(5,50,25,25);
  SetRGBPalette(6,50,30,30);
  SetRGBPalette(7,50,40,40);
  SetRGBPalette(8,50,50,50);
  SetRGBPalette(9,50,50,60);
  SetRGBPalette(10,45,45,60);
  SetRGBPalette(11,40,40,60);
  SetRGBPalette(12,35,35,60);
  SetRGBPalette(13,40,40,40);
  SetRGBPalette(14,50,50,50);

{  for i := 1 to 15 do
  begin
    SetColor(i);
    SetFillStyle(1,i);
    Bar(50,200+10*i,150,210+10*i);
  end;}
end; { Palette }



{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴 Main Program 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
begin
  Randomize;
  Initialize;
  SetViewPort(0,0,MaxX,MaxY,ClipOn);
  Palette;
  for i := 1 to 1000 do
    PutPixel(Random(MaxX),Random(MaxX),3+Random(8));
  Galaxy(200,100,80.0,0.2,1.0,Left);
  Galaxy(400,300,30.0,0.5,1.0,Right);
  Galaxy(100,380,15.0,0.2,1.0,Left);
  Galaxy(500,80,25.0,0.2,1.0,Right);
  Galaxy(600,400,8.0,0.2,1.0,Left);
  InitStars;
  repeat
    for i := 1 to MaxStars do
    with Stars[i]^ do
    begin
      MoveStar;
      CheckStar;
    end;
    {Delay(10);}
  until KeyPressed;
  DoneStars;
  CloseGraph;
end. { Space }
