program BookLib;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� INFO 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{� File    : BOOKLIB.PAS                                                    �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : Book Library.                                                  �}
{� Updated : April 22 1994                                                  �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$A+   Word align data                                                       }
{$B-   Short-circuit Boolean expression evaluation                           }
{$E-   Disable linking with 8087-emulating run-time library                  }
{$G+   Enable 80286 code generation                                          }
{$R-   Disable generation of range-checking code                             }
{$S-   Disable generation of stack-overflow checking code                    }
{$V-   String variable checking                                              }
{$X+   Enable Turbo Pascal's extended syntax                                 }
{$N+   80x87 code generation                                                 }
{$D-   Disable generation of debug information                               }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

uses
  UScreen,   { HT Screen routines   }
  UCommon,   { HT Common routines   }
  UKeys,     { HT Keyboard routines }
  UStrs,     { HT String routines   }
  Dos;       { TP DOS routines      }

const
  MaxBooks = 14000;
  EntryA1  = White+DarkGrayBG;
  EntryA2  = DarkGray+LightGrayBG;
  EntryA3  = White+CyanBG;
  EntryA4  = White+MagentaBG;
  Letters  : string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ뮑�';
  EntryRow : byte = 5;
  EntryCol : byte = 3;

type
  PBookEntry = ^TBookEntry;
  TBookEntry = record
    Author   : string[30];
    Publisher: string[30];
    Year     : string[4];
    Title    : string[56];
    Pages    : integer;
    ISBN     : string[15];
  end;

var
  OneEntry    : PBookEntry;
  BookFile    : file of TBookEntry;
  NumBooks    : integer;
  BookList    : array[1..MaxBooks] of PBookEntry;
  Filename    : string;
  Dir         : DirStr;
  Name        : NameStr;
  Ext         : ExtStr;


procedure DrawLetters(C: char);
var
  i,p: byte;
begin
  C := Upcase( C);
  Fill( EntryRow+2, EntryCol+3, 1, 70, EntryA1, ' ');
  p := Pos( c, Letters);
  if p > 1 then
  for i := 1 to p-1 do
    WriteChar( EntryRow+2, EntryCol+3+2*i, EntryA1, Letters[i]);

  if p < 29 then
  for i := (p+1) to 29 do
    WriteChar( EntryRow+2, EntryCol+5+2*i, EntryA1, Letters[i]);

  WriteStr( EntryRow+2, EntryCol+3+2*p, EntryA2, ' '+Letters[p]+' ');
  WriteChar( EntryRow+2, EntryCol+6+2*p, EntryA1 and $F0, '�');
end;


procedure WriteStatusLine(Num: byte);
begin
  Fill( CRTRows, 1, 1, 80, White+CyanBG, ' ');
  WriteStr( CRTRows, 80-Length( Filename), SameAttr, Filename);
  if Num = 1 then
  begin
    WriteStr( CRTRows, 2, Yellow+CyanBG, 'F1');
    WriteEos( White+CyanBG, '-Help  ');
    WriteEos( Yellow+CyanBG, 'F2');
    WriteEos( White+CyanBG, '-Save  ');
    WriteEos( Yellow+CyanBG, 'F3');
    WriteEos( White+CyanBG, '-Open  ');
    WriteEos( Yellow+CyanBG, 'F8');
    WriteEos( White+CyanBG, '-Edit  ');
    WriteEos( Yellow+CyanBG, 'Esc');
    WriteEos( White+CyanBG, '-Quit');
    Exit;
  end;
  if Num = 2 then
  begin
    WriteStr( CRTRows, 2, Yellow+CyanBG, #24+#25);
    WriteEos( White+CyanBG, '-Select  ');
    WriteEos( Yellow+CyanBG, 'F8');
    WriteEos( White+CyanBG, '-Save Changes  ');
    WriteEos( Yellow+CyanBG, 'Esc');
    WriteEos( White+CyanBG, '-Disregard Changes');
    WriteStr( CRTRows, 80-Length( Filename), SameAttr, Filename);
    Exit;
  end;
  if Num = 3 then
  begin
    WriteStr( CRTRows, 2, Yellow+CyanBG, #24+#25);
    WriteEos( White+CyanBG, '-Select  ');
    WriteEos( Yellow+CyanBG, 'Return');
    WriteEos( White+CyanBG, '-Goto Record  ');
    WriteEos( Yellow+CyanBG, 'Esc');
    WriteEos( White+CyanBG, '-Close Index');
    Exit;
  end;
end;


procedure Glider(Row, Col, Rows, Num, MaxNum: byte);
var
  b: byte;
begin
  WriteChar( Row, Col, White+BlackBG, #24);
  WriteChar( Row+Rows-1, Col, White+BlackBG, #25);
  Fill( Row+1, Col, Rows-2, 1, White+BlackBG, '�');
  if (Num<=MaxNum) then
  begin
    b := 1+Round((Rows-3)*(Num/MaxNum));
    WriteChar( Row+b, Col, White+BlackBG, '�');
  end;
end;


procedure BlankEntries;
begin
  WriteStr( EntryRow+3, EntryCol+5, EntryA2, 'Author');
  Fill( EntryRow+4, EntryCol+5, 1, 32, EntryA3, ' ');

  WriteStr( EntryRow+3, EntryCol+38, EntryA2, 'Publisher');
  Fill( EntryRow+4, EntryCol+38, 1, 32, EntryA3, ' ');

  WriteStr( EntryRow+5, EntryCol+5, EntryA2, 'Year');
  Fill( EntryRow+6, EntryCol+5, 1, 6, EntryA3, ' ');

  WriteStr(EntryRow+5,EntryCol+12, EntryA2, 'Title');
  Fill( EntryRow+6, EntryCol+12, 1, 58, EntryA3, ' ');

  WriteStr( EntryRow+7, EntryCol+5, EntryA2, 'ISBN');
  Fill( EntryRow+8, EntryCol+5, 1, 17, EntryA3, ' ');

  WriteStr( EntryRow+7, EntryCol+23, EntryA2, 'Pages');
  Fill( EntryRow+8, EntryCol+23, 1, 7, EntryA3, ' ');

  WriteStr( EntryRow+7, EntryCol+54, EntryA2, 'Record');
  Fill( EntryRow+8, EntryCol+54, 1, 16, EntryA3, ' ');
end;


procedure MainBackground;

  procedure Hole(R,C: byte);
  begin
    WriteStr( R, C, EntryA2 and $F0, '複複');
    WriteStr( R+1, C, (EntryA2 and $F0) or ((EntryA1 shr 4) and $0F), '賽賽');
    WriteChar( R+1, C, EntryA2 and $F0, '�');
    WriteChar( R+1, C+2, White, '�');
    WriteChar( R+2, C+2, White, '�');
  end;

begin
  Fill(1,1,1,80,White+BlueBG,' ');
  WriteC(1,40,SameAttr,'Book Library');
  Box( EntryRow, EntryCol, 14, 75, EntryA1, DoubleBorder, ' ');
  AddShadow( EntryRow, EntryCol, 14, 75);
  Fill( EntryRow+3, EntryCol+3, 9, 69, EntryA2, ' ');
  AddSmallShadow( EntryRow+3, EntryCol+3, 9, 69);
  Hole( EntryRow+10, EntryCol+5);
  Hole( EntryRow+10, EntryCol+25);
  Hole( EntryRow+10, EntryCol+45);
  Hole( EntryRow+10, EntryCol+66);
  BlankEntries;
end;


procedure Init(EntryNum: integer);
var
  i: byte;
begin
  with BookList[EntryNum]^ do
  begin
    Author := '                              ';
    Title  := '                                                        ';
    Title[0] := #0;
    Year   := '    ';
    Year[0] := #0;
    Publisher:= '                              ';
    Publisher[0] := #0;
    ISBN := '               ';
    ISBN[0] := #0;
    Pages := 0;
  end;
end;


procedure Draw(EntryNum: integer);
var
  c: char;
  i: byte;
begin
  with BookList[EntryNum]^ do
  begin
    c := Author[1];
    DrawLetters( c);
    BlankEntries;
    WriteStr(EntryRow+4, EntryCol+6, EntryA3, Author);
    WriteStr(EntryRow+4, EntryCol+39, EntryA3, Publisher);
    WriteStr(EntryRow+6, EntryCol+6, EntryA3, Year);
    WriteStr(EntryRow+6, EntryCol+13, EntryA3, Title);
    WriteStr(EntryRow+8, EntryCol+6, EntryA3, ISBN);
    WriteStr(EntryRow+8, EntryCol+24, EntryA3, StrLF(Pages,5));
    WriteStr(EntryRow+8, EntryCol+54, EntryA3, StrLF(EntryNum,5)+' of '+StrLF(NumBooks,5));
  end;
end;


procedure Edit(Num: integer);
var
  Current: byte;
  s: string;
begin
  Current := 1;
  BlankEntries;
  Draw( Num);
  WriteStatusLine(2);
  with BookList[Num]^ do
  begin
    repeat
      case Current of
        1: begin
             s := Author;
             InputStr( s, EntryRow+4, EntryCol+6, SizeOf(Author)-1,
               EntryA4, [Return, Escape, UpArrow, DownArrow, AltC, F8]);
             FillAt( EntryRow+4, EntryCol+6, 1, SizeOf(Author)-1, EntryA3);
             Author := s;
           end;
        2: begin
             s := Publisher;
             InputStr( s, EntryRow+4, EntryCol+39, SizeOf(Publisher)-1,
               EntryA4, [Return, Escape, UpArrow, DownArrow, AltC, F8]);
             FillAt( EntryRow+4, EntryCol+39, 1, SizeOf(Publisher)-1, EntryA3);
             Publisher := s;
           end;
        3: begin
             s := Year;
             InputStr( s, EntryRow+6, EntryCol+6, SizeOf(Year)-1,
               EntryA4, [Return, Escape, UpArrow, DownArrow, AltC, F8]);
             FillAt( EntryRow+6, EntryCol+6, 1, SizeOf(Year)-1, EntryA3);
             Year := s;
           end;
        4: begin
             s := Title;
             InputStr( s, EntryRow+6, EntryCol+13, SizeOf(Title)-1,
               EntryA4, [Return, Escape, UpArrow, DownArrow, AltC, F8]);
             FillAt( EntryRow+6, EntryCol+13, 1, SizeOf(Title)-1, EntryA3);
             Title := s;
           end;
        5: begin
             s := ISBN;
             InputStr( s, EntryRow+8, EntryCol+6, SizeOf(ISBN)-1,
               EntryA4, [Return, Escape, UpArrow, DownArrow, AltC, F8]);
             FillAt( EntryRow+8, EntryCol+6, 1, SizeOf(ISBN)-1, EntryA3);
             ISBN := s;
           end;
        6: begin
             s := StrL(Pages);
             InputStr( s, EntryRow+8, EntryCol+24, 5,
               EntryA4, [Return, Escape, UpArrow, DownArrow, AltC, F8]);
             FillAt( EntryRow+8, EntryCol+24, 1, 5, EntryA3);
             Pages := ValInt(s);
           end;
      end;
      case Key of
        UpArrow  : if Current > 1 then Dec(Current);
        Return,
        DownArrow: if Current < 6 then Inc(Current);
        AltC     : begin
                     Init( Num);
                     Draw( Num);
                   end;
      end;
    until Key in [Escape,F8];
  end;
  WriteStatusLine(1);
end;


procedure ReadFromFile(Filename: string);
var
  i: integer;
begin
  NumBooks := 0;
  {$I-}
  Assign( BookFile, FileName);
  Reset( BookFile);
  {$I+}
  if (IOResult<>0) or (Filename='') then
  begin
    MessageBox( 'No file named '+Filename);
    Exit;
  end;

  NumBooks := FileSize( BookFile);
  if NumBooks = 0 then
  begin
    MessageBox( 'Error loading file');
    Close( BookFile);
    Exit;
  end;

  for i := 1 to NumBooks do
  begin
    BookList[i] := New( PBookEntry);
    Read( BookFile, BookList[i]^);
  end;
  Close( BookFile);
end;


procedure SaveToFile(Filename: string; var Changed: boolean);
var
  i: integer;
begin
  Assign( BookFile, FileName);
  Rewrite( BookFile);
  if NumBooks > 0 then
  for i := 1 to NumBooks do
    Write( BookFile, BookList[i]^);
  Close( BookFile);
  Changed := false;
end;


procedure AskFilename(var Filename: string);
var
  p: pointer;
begin
  GetMem( p, 2*7*52);
  StoreToMem( 10, 15, 7, 52, p^);
  Box( 10, 15, 6, 50, White+BlueBG, SingleBorder, ' ');
  AddShadow( 10, 15, 6, 50);
  Fill( 11, 16, 1, 48, Blue+WhiteBG, ' ');
  WriteC( 11, 40, SameAttr, 'Save');
  WriteStr( 13, 18, SameAttr, 'Filename:');
  InputStr( Filename, 13, 28, 32, Yellow+LightGrayBG, [Return,Escape]);
  StoreToScr( 10, 15, 7, 52, p^);
  FreeMem( p, 2*7*52);
end;


procedure SortList(var EntryNum: integer);
var
  i,j: integer;
  b: boolean;
  t: PBookEntry;
begin
  b := true;
  if NumBooks > 1 then
  repeat
    b := true;
    for i := 1 to NumBooks-1 do
    if UpcaseStr(BookList[i]^.Author+BookList[i]^.Year) > UpcaseStr(BookList[i+1]^.Author+BookList[i+1]^.Year) then
    begin
      t := BookList[i];
      BookList[i] := BookList[i+1];
      BookList[i+1] := t;
      b := false;
      if EntryNum = i then EntryNum := i+1
      else if EntryNum = i+1 then EntryNum := i;
    end;
  until b;
end;


procedure DeleteFromList(EntryNum: integer);
var
  i: integer;
begin
  if EntryNum = NumBooks then
    Dispose( BookList[NumBooks])
  else
  begin
    OneEntry := BookList[EntryNum];
    for i := EntryNum to NumBooks-1 do
      BookList[i] := BookList[i+1];
    BookList[NumBooks] := OneEntry;
    Dispose( BookList[NumBooks])
  end;
  Dec( NumBooks);
end;


procedure Index(var EntryNum: integer);
const
  Row= 4;
  Col= 20;
var
  s,i,j: integer;
  p: pointer;

  procedure WriteName( Row, i: integer);
  begin
    with BookList[i]^ do
    begin
      WriteStr( Row, Col+2, SameAttr, Author);
      WriteStr( Row, Col+39, SameAttr, '('+Year+')');
    end;
  end;

  procedure WritePage( Start: integer);
  var
    i: byte;
  begin
    Fill( Row+3, Col+1, 15, 45, White+MagentaBG,' ');
    for i := 1 to 15 do
      if Start+i-1 <= NumBooks then
        WriteName( Row+i+2, Start+i-1);
  end;

begin
  WriteStatusLine(3);
  GetMem( p, 2*20*50);
  StoreToMem( Row, Col, 20, 50, p^);
  Box( Row, Col, 19, 48, White+MagentaBG, SingleBorder, ' ');
  AddShadow( Row, Col, 19, 48);
  Fill( Row+1, Col+1, 1, 46, Magenta+WhiteBG,' ');
  WriteStr( Row+1, Col+21, SameAttr, 'Index');
  i := EntryNum;
  s := EntryNum;
  WritePage(i);
  repeat
    if NumBooks > 1 then
      Glider( Row+2, Col+47, 16, i-1, NumBooks-1);
    FillAt(Row+i-s+3, Col+1, 1, 45, Yellow+BlackBG);
    InKey( Ch, Key);
    FillAt(Row+i-s+3, Col+1, 1, 45, White+MagentaBG);
    case Key of
      UpArrow  : if i > 1 then
                 begin
                   Dec(i);
                   if i < s then
                   begin
                     Dec(s);
                     ScrollDown(Row+3, Col+1, 15, 44, White+MagentaBG);
                     WriteName( Row+3, i);
                   end;
                 end;
      DownArrow: if i < NumBooks then
                 begin
                   Inc(i);
                   if i > (s+14) then
                   begin
                     Inc(s);
                     ScrollUp(Row+3, Col+1, 15, 44, White+MagentaBG);
                     WriteName( Row+17, i);
                   end;
                 end;
      PgUp     : if i > 15 then
                 begin
                   Dec(i,15);
                   Dec(s,15);
                   if s < 1 then s := 1;
                   WritePage(s);
                 end;
      PgDn     : if (s+15) <= NumBooks then
                 begin
                   Inc(i,15);
                   if i > NumBooks then i := NumBooks;
                   Inc(s,15);
                   WritePage(s);
                 end;
      HomeKey  : if i > 1 then
                 begin
                   i := 1;
                   s := 1;
                   WritePage(s);
                 end;
      EndKey  :  if i < NumBooks then
                 begin
                   i := NumBooks;
                   s := i - 14;
                   if s < 1 then s := 1;
                   WritePage(s);
                 end;
    end;
  until Key in [Escape, Return];
  if Key=Return then
    EntryNum := i;
  Key := NullKey;
  StoreToScr( Row, Col, 20, 50, p^);
  FreeMem( p, 2*20*50);
  WriteStatusLine(1);
end;


procedure FindLetter(c: char;  var EntryNum: integer);
var
  i: integer;
begin
  i := 0;
  case c of
    '�' : c := '�';
    '�' : c := '�';
    '�' : c := '�';
  else
    c := Upcase(c);
  end;
  if NumBooks <= 0 then Exit;
  repeat
    Inc(i);
  until (BookList[i]^.Author[1]>=c) or (i>=NumBooks);
  EntryNum := i;
end;


function Search(var SearchString: string;  var EntryNum: integer;  Again: boolean): boolean;
var
  i    : integer;
  j,k,l: byte;
  Found: boolean;

  procedure AskString(var SearchString: string);
  var
    p: pointer;
    s: string;
  begin
    s := SearchString;
    GetMem( p, 2*7*52);
    StoreToMem( 10, 15, 7, 52, p^);
    Box( 10, 15, 6, 50, White+BlueBG, SingleBorder, ' ');
    AddShadow( 10, 15, 6, 50);
    Fill( 11, 16, 1, 48, Blue+WhiteBG, ' ');
    WriteC( 11, 40, SameAttr, 'Search String');
    WriteStr( 13, 18, SameAttr, 'String:');
    InputStr( s, 13, 28, 32, Yellow+LightGrayBG, [Return,Escape]);
    StoreToScr( 10, 15, 7, 52, p^);
    FreeMem( p, 2*7*52);
    if Key = Return then
      SearchString := s;
  end;

begin
  if Again then
    i := EntryNum
  else begin
    i := 0;
    AskString( SearchString);
    if Key = Escape then
    begin
      Key := NullKey;
      Exit;
    end;
  end;
  if i = NumBooks then
    Exit;
  SearchString := UpcaseStr( SearchString);
  l := Length( SearchString);
  Found := false;
  repeat
    Inc(i);
    with BookList[i]^ do
    begin
      if Pos(SearchString, UpcaseStr(Author)) > 0 then Found := true;
      if Pos(SearchString, UpcaseStr(Publisher)) > 0 then Found := true;
      if Pos(SearchString, UpcaseStr(Year)) > 0 then Found := true;
      if Pos(SearchString, UpcaseStr(Title)) > 0 then Found := true;
    end;
  until Found or (i >= NumBooks);
  if Found then
    EntryNum := i;
  Search := Found;
end;


procedure Help;
const
  HRow = 3;
  HCol = 20;
  HRows= 21;
  HCols= 40;
var
  p: pointer;
begin
  GetMem(p, 2*HRows*HCols);
  StoreToMem( HRow, HCol, HRows, HCols, p^);

  Box( HRow, HCol, HRows-1, HCols-2, White+GreenBG, SingleBorder, ' ');
  AddShadow( HRow, HCol, HRows-1, HCols-2);
  Fill(HRow+1, HCol+1, 1, HCols-4, Green+WhiteBG, ' ');
  WriteC(HRow+1, HCol+(HCols div 2)-2, SameAttr, 'Help');
  WriteStr( HRow+3, HCol+3, LightCyan+GreenBG, 'F1    ');
    WriteEos( SameAttr, '- This help');
  WriteStr( HRow+4, HCol+3, LightCyan+GreenBG, 'F2    ');
    WriteEos( SameAttr, '- Save library to file');
  WriteStr( HRow+5, HCol+3, LightCyan+GreenBG, 'F3    ');
    WriteEos( SameAttr, '- Load library from file');
  WriteStr( HRow+6, HCol+3, LightCyan+GreenBG, 'F8    ');
    WriteEos( SameAttr, '- Edit current entry');
  WriteStr( HRow+7, HCol+3, LightCyan+GreenBG, #27+#26+'    ');
    WriteEos( SameAttr, '- Select entry');
  WriteStr( HRow+8, HCol+3, LightCyan+GreenBG, 'Alt-I ');
    WriteEos( SameAttr, '- Show artist index');
  WriteStr( HRow+9, HCol+3, LightCyan+GreenBG, 'Alt-S ');
    WriteEos( SameAttr, '- Search for string');
  WriteStr( HRow+10, HCol+3, LightCyan+GreenBG, 'Alt-N ');
    WriteEos( SameAttr, '- Find next occurence');
  WriteStr( HRow+11, HCol+3, LightCyan+GreenBG, 'Alt-T ');
    WriteEos( SameAttr, '- Write list to text file');
  WriteStr( HRow+12, HCol+3, SameAttr, '        BOOKLIB.TXT');
  WriteStr( HRow+13, HCol+3, LightCyan+GreenBG, 'Ins   ');
    WriteEos( SameAttr, '- Insert and edit new entry');
  WriteStr( HRow+14, HCol+3, LightCyan+GreenBG, 'Del   ');
    WriteEos( SameAttr, '- Delete current entry');
  WriteStr( HRow+15, HCol+3, LightCyan+GreenBG, 'Esc   ');
    WriteEos( SameAttr, '- Quit BookLibrary');
  PushButton( HRow+HRows-4, HCol+(HCols div 2)-4, Green+WhiteBG, White+GreenBG, #16+' Ok '+#17, false);
  Key := NullKey;

  StoreToScr( HRow, HCol, HRows, HCols, p^);
  FreeMem(p, 2*HRows*HCols);
end;


procedure WriteToText;
var
  i: integer;
  f: text;
  p: pointer;
begin
  FSplit( Filename, Dir, Name, Ext);
  GetMem( p, 2*7*20);
  StoreToMem( 10, 30, 7, 20, p^);
  Box( 10, 30, 6, 18, White+RedBG, SingleBorder, ' ');
  AddShadow( 10, 30, 6, 18);
  Fill( 11, 31, 1, 16, Red+WhiteBG, ' ');
  WriteC( 11, 38, SameAttr, 'Text Save');
  WriteStr( 13, 32, SameAttr, 'Writing ');
  Assign( f, Name+'.TXT');
  Rewrite( f);
  for i := 1 to NumBooks do
  with BookList[i]^ do
  begin
    WriteStr( 13, 41, SameAttr, StrLF( i, 5));
    Write( f, '"'+Author+'"');
    Write( f, ',"'+Publisher+'"');
    Write( f, ',"'+Year+'"');
    Write( f, ',"'+Title+'"');
    Write( f, ',"'+ISBN+'"');
    Write( f, ',"',Pages,'"');
    WriteLn( f);
  end;
  Close( f);
  StoreToScr( 10, 30, 7, 20, p^);
  FreeMem( p, 2*7*20);
end;


procedure MainMenu;
var
  EntryNum  : integer;
  SearchString,
  s,OldName : string;
  Changed,
  Scrolled  : boolean;
begin
  Fill( 1, 1, CRTRows, 80, White+BlueBG, '�');
  WriteStatusLine(1);
  MainBackground;
  OneEntry := New( PBookEntry);
  DrawLetters('A');
  NumBooks := 0;
  EntryNum := 0;
  Scrolled := false;
  Changed := false;
  SearchString := '';
  Key := NullKey;
  repeat
    if not Scrolled then
      Inkey( Ch, Key)
    else
      Scrolled := false;
    case Key of
      AltT  : WriteToText;
      AltI  : if NumBooks > 0 then
              begin
                Index( EntryNum);
                Draw( EntryNum);
              end;
      AltS  : if NumBooks > 0 then
              begin
                if Search(SearchString, EntryNum, false) then
                  Draw( EntryNum)
                else
                  MessageBox('String not found');
                Key := NullKey;
              end;
      AltN  : if NumBooks > 0 then
              begin
                if Search(SearchString, EntryNum, true) then
                  Draw( EntryNum)
                else
                  MessageBox('String not found');
                Key := NullKey;
              end;
      AltT  : WriteToText;
      InsKey: if NumBooks < MaxBooks then
              begin
                Inc( NumBooks);
                BookList[NumBooks] := New( PBookEntry);
                if NumBooks > 1 then
                  BookList[NumBooks]^ := BookList[EntryNum]^
                else Init( NumBooks);
                Edit( NumBooks);
                if Key=Escape then
                begin
                  Dispose( BookList[NumBooks]);
                  Dec( NumBooks);
                end
                else
                begin
                  EntryNum := NumBooks;
                  SortList( EntryNum);
                  Changed := true;
                end;
                if EntryNum > 0 then
                  Draw( EntryNum);
                Key := NullKey;
              end
              else
                MessageBox('Too many entries in one file');
      DelKey: if Confirm ('Delete record '+BookList[EntryNum]^.Author,false) then
              begin
                DeleteFromList( EntryNum);
                if EntryNum > NumBooks then
                  EntryNum := NumBooks;
                Draw( EntryNum);
                Changed := true;
              end;
      F1    : Help;
      F2    : if Changed and (NumBooks > 0) then
              begin
                AskFilename( Filename);
                if Key=Return then
                begin
                  if FileExists( Filename) then
                  begin
                    if Confirm('File exists!  Overwrite '+Filename, false) then
                      SaveToFile( Filename, Changed);
                  end
                  else SaveToFile( Filename, Changed);
                end;
                Key := NullKey;
              end;
      F3    : begin
                if Changed then
                  if Confirm('File changed. Save first', true) then
                    SaveToFile( Filename, Changed);
                OldName := Filename;
                OpenFile( 4, 15, Filename);
                if Filename <> OldName then
                begin
                  ReadFromFile( Filename);
                  EntryNum := 0;
                  BlankEntries;
                  if NumBooks > 0 then
                  begin
                    EntryNum := 1;
                    Draw( EntryNum);
                  end;
                end;
                WriteStatusLine(1);
                Key := NullKey;
              end;
      F8    : if NumBooks > 0 then
              begin
                OneEntry^ := BookList[EntryNum]^;
                Edit( EntryNum);
                if Key=F8 then
                begin
                  SortList( EntryNum);
                  Changed := true;
                end
                else
                  BookList[EntryNum]^ := OneEntry^;
                Draw( EntryNum);
                Key := NullKey;
              end;
      TextKey:if NumBooks > 0 then
              begin
                FindLetter( Ch, EntryNum);
                Draw( EntryNum);
                Key := NullKey;
              end;
      RightArrow: if EntryNum < NumBooks then
              begin
                Inc( EntryNum);
                Draw( EntryNum);
                Key := NullKey;
              end;
      LeftArrow: if EntryNum > 1 then
              begin
                Dec( EntryNum);
                Draw( EntryNum);
                Key := NullKey;
              end
              else Key := NullKey;
    end;
  until Key=Escape;
  if Changed then
    if Confirm('File has changed. Save',true) then
      SaveToFile( Filename, Changed);
  for EntryNum := 1 to NumBooks do
    Dispose( BookList[EntryNum]);
  ClrScr;
end;


begin
  GetDir( 0, CurrentPath);
  if Length( CurrentPath) > 3 then
    CurrentPath := CurrentPath + '\';
  SearchPath := '*.BOK';
  Filename := '*.*';
  SetIntens;
  SetCursor( CursorOff);

  MainMenu;

  SetBlink;
  SetCursor( CursorUnderline);
end.