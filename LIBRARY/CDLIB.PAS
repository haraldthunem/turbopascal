program CDLib;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� INFO 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{� File    : CDLIB.PAS                                                      �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : Compact Disc Library.                                          �}
{� Updated : May 21 1994                                                    �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$A+   Word align data                                                       }
{$B-   Short-circuit Boolean expression evaluation                           }
{$E-   Disable linking with 8087-emulating run-time library                  }
{$G+   Enable 80286 code generation                                          }
{$R-   Disable generation of range-checking code                             }
{$S-   Disable generation of stack-overflow checking code                    }
{$V-   String variable checking                                              }
{$X+   Enable Turbo Pascal's extended syntax                                 }
{$N+   80x87 code generation                                                 }
{$D-   Disable generation of debug information                               }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

uses
  Dos,       { TP Dos routines      }
  UScreen,   { HT Screen routines   }
  UCommon,   { HT Common routines   }
  UKeys,     { HT Keyboard routines }
  UStrs;     { HT String routines   }

const
  MaxCD    = 1000;
  MaxSongs = 20;
  EntryA1  = White+DarkGrayBG;
  EntryA2  = DarkGray+LightGrayBG;
  EntryA3  = White+CyanBG;
  EntryA4  = White+MagentaBG;
  Letters  : string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ뮑�';
  EntryRow : byte = 2;
  EntryCol : byte = 3;

type
  PCDEntry = ^TCDEntry;
  TCDEntry = record
    Artist : string[30];
    Title  : string[56];
    Year   : string[4];
    Company: string[30];
    Start  : byte;
    Song   : array[1..MaxSongs] of String[50];
    Time   : array[1..MaxSongs] of String[5];
  end;

var
  OneEntry    : PCDEntry;
  CDFile      : file of TCDEntry;
  NumCD       : integer;
  CDList      : array[1..MaxCD] of PCDEntry;
  Filename    : string;


procedure DrawLetters(C: char);
var
  i,p: byte;
begin
  C := Upcase( C);
  Fill( EntryRow+2, EntryCol+3, 1, 70, EntryA1, ' ');
  p := Pos( c, Letters);
  if p > 1 then
  for i := 1 to p-1 do
    WriteChar( EntryRow+2, EntryCol+3+2*i, EntryA1, Letters[i]);

  if p < 29 then
  for i := (p+1) to 29 do
    WriteChar( EntryRow+2, EntryCol+5+2*i, EntryA1, Letters[i]);

  WriteStr( EntryRow+2, EntryCol+3+2*p, EntryA2, ' '+Letters[p]+' ');
  WriteChar( EntryRow+2, EntryCol+6+2*p, EntryA1 and $F0, '�');
end;


procedure BlankEntries;
begin
  WriteStr( EntryRow+3, EntryCol+5, EntryA2, 'Artist/Group');
  Fill( EntryRow+4, EntryCol+5, 1, 32, EntryA3, ' ');

  WriteStr( EntryRow+3, EntryCol+38, EntryA2, 'Company');
  Fill( EntryRow+4, EntryCol+38, 1, 32, EntryA3, ' ');

  WriteStr( EntryRow+5, EntryCol+5, EntryA2, 'Year');
  Fill( EntryRow+6, EntryCol+5, 1, 6, EntryA3, ' ');

  WriteStr(EntryRow+5,EntryCol+12, EntryA2, 'Title');
  Fill( EntryRow+6, EntryCol+12, 1, 58, EntryA3, ' ');

  WriteStr(EntryRow+7,EntryCol+5, EntryA2, 'Songs');
  Fill( EntryRow+8, EntryCol+5, 10, 55, EntryA3, ' ');

  WriteStr(EntryRow+7,EntryCol+61, EntryA2, 'Time');
  Fill( EntryRow+8, EntryCol+61, 10, 7, EntryA3, ' ');

  Fill( EntryRow+9, EntryCol+69, 8, 1, White+BlackBG, '�');
  WriteChar( EntryRow+8, EntryCol+69, White+BlackBG, #24);
  WriteChar( EntryRow+17, EntryCol+69, White+BlackBG, #25);
end;


procedure WriteStatusLine(Num: byte);
begin
  if Num = 1 then
  begin
    Fill( CRTRows, 1, 1, 80, White+CyanBG, ' ');
    WriteStr( CRTRows, 2, Yellow+CyanBG, 'F1');
    WriteEos( White+CyanBG, '-Help  ');
    WriteEos( Yellow+CyanBG, 'F2');
    WriteEos( White+CyanBG, '-Save  ');
    WriteEos( Yellow+CyanBG, 'F3');
    WriteEos( White+CyanBG, '-Open  ');
    WriteEos( Yellow+CyanBG, 'F8');
    WriteEos( White+CyanBG, '-Edit  ');
    WriteEos( Yellow+CyanBG, 'Esc');
    WriteEos( White+CyanBG, '-Quit');
    Exit;
  end;
  if Num = 2 then
  begin
    Fill( CRTRows, 1, 1, 80, White+CyanBG, ' ');
    WriteStr( CRTRows, 2, Yellow+CyanBG, #24+#25);
    WriteEos( White+CyanBG, '-Select  ');
    WriteEos( Yellow+CyanBG, 'F8');
    WriteEos( White+CyanBG, '-Save Changes  ');
    WriteEos( Yellow+CyanBG, 'Esc');
    WriteEos( White+CyanBG, '-Disregard Changes');
    Exit;
  end;
  if Num = 3 then
  begin
    Fill( CRTRows, 1, 1, 80, White+CyanBG, ' ');
    WriteStr( CRTRows, 2, Yellow+CyanBG, #24+#25);
    WriteEos( White+CyanBG, '-Select  ');
    WriteEos( Yellow+CyanBG, 'Return');
    WriteEos( White+CyanBG, '-Goto Record  ');
    WriteEos( Yellow+CyanBG, 'Esc');
    WriteEos( White+CyanBG, '-Close Index');
    Exit;
  end;
end;


procedure Glider(Row, Col, Rows, Num, MaxNum: byte);
var
  b: byte;
begin
  WriteChar( Row, Col, White+BlackBG, #24);
  WriteChar( Row+Rows-1, Col, White+BlackBG, #25);
  Fill( Row+1, Col, Rows-2, 1, White+BlackBG, '�');
  if (Num<=MaxNum) then
  begin
    b := 1+Round((Rows-3)*(Num/MaxNum));
    WriteChar( Row+b, Col, White+BlackBG, '�');
  end;
end;


procedure MainBackground;
var
  Dir : DirStr;
  Name: NameStr;
  Ext : ExtStr;

  procedure Hole(R,C: byte);
  begin
    WriteStr( R, C, EntryA2 and $F0, '複複');
    WriteStr( R+1, C, (EntryA2 and $F0) or ((EntryA1 shr 4) and $0F), '賽賽');
    WriteChar( R+1, C, EntryA2 and $F0, '�');
    WriteChar( R+1, C+2, White, '�');
    WriteChar( R+2, C+2, White, '�');
  end;

begin
  FSplit( Filename, Dir, Name, Ext);
  Box( EntryRow, EntryCol, 22, 75, EntryA1, DoubleBorder, ' ');
  AddShadow( EntryRow, EntryCol, 22, 75);
  if Filename <> '' then
    WriteC( EntryRow, 39, SameAttr, ' '+Name+Ext+' ')
  else WriteC( EntryRow, 39, SameAttr, ' CD Library ');
  Fill( EntryRow+3, EntryCol+3, 17, 69, EntryA2, ' ');
  AddSmallShadow( EntryRow+3, EntryCol+3, 17, 69);
  Hole( EntryRow+18, EntryCol+5);
  Hole( EntryRow+18, EntryCol+25);
  Hole( EntryRow+18, EntryCol+45);
  Hole( EntryRow+18, EntryCol+66);
  BlankEntries;
end;


procedure Init(EntryNum: integer);
var
  i: byte;
begin
  with CDList[EntryNum]^ do
  begin
    Artist := '                              ';
    Title  := '                                                        ';
    Title[0] := #0;
    Year   := '    ';
    Year[0] := #0;
    Start := 1;
    Company:= '                              ';
    Company[0] := #0;
    for i := 1 to MaxSongs do
    begin
      Song[i] := '                                                  ';
      Song[i,0] := #0;
      Time[i] := '     ';
      Time[i,0] := #0;
    end;
  end;
end;


procedure WriteSong(EntryNum: integer; WRow, SongNumber: byte);
begin
  with CDList[EntryNum]^ do
  begin
    FillAt( EntryRow+7+WRow, EntryCol+9, 1, SizeOf(Song[1])-1, EntryA3);
    WriteStr( EntryRow+7+WRow, EntryCol+6, EntryA3, StrLF(SongNumber, 2)+'.'+Song[SongNumber]);
    FillAt( EntryRow+7+WRow, EntryCol+62, 1, SizeOf(Time[1])-1, EntryA3);
    WriteStr( EntryRow+7+WRow, EntryCol+62, EntryA3, Time[SongNumber]);
  end;
end;


procedure Draw(EntryNum: integer);
var
  c: char;
  i: byte;
begin
  with CDList[EntryNum]^ do
  begin
    c := Artist[1];
    DrawLetters( c);
    BlankEntries;
    WriteStr(EntryRow+4, EntryCol+6, EntryA3, Artist);
    WriteStr(EntryRow+4, EntryCol+39, EntryA3, Company);
    WriteStr(EntryRow+6, EntryCol+6, EntryA3, Year);
    WriteStr(EntryRow+6, EntryCol+13, EntryA3, Title);
    for i := Start to Start+9 do
      WriteSong( EntryNum, i-Start+1, i);
  end;
end;


procedure Scroll(EntryNum: integer);
var
  i: byte;
begin
  Draw( EntryNum);
  with CDList[EntryNum]^ do
  begin
    repeat
      case Key of
        DownArrow: if Start < 11 then
          begin
            ScrollUp( EntryRow+8, EntryCol+5, 10, 55, EntryA3);
            ScrollUp( EntryRow+8, EntryCol+62, 10, 5, EntryA3);
            Inc(Start);
            WriteSong( EntryNum, 10, Start+9);
          end;
        UpArrow  : if Start > 1 then
          begin
            ScrollDown( EntryRow+8, EntryCol+5, 10, 55, EntryA3);
            ScrollDown( EntryRow+8, EntryCol+62, 10, 5, EntryA3);
            Dec(Start);
            WriteSong( EntryNum, 1, Start);
          end;
      end;
      Glider( EntryRow+8, EntryCol+69, 10, Start-1, 10);
      if Key in [UpArrow,DownArrow] then
        InKey( Ch, Key);
    until not (Key in [UpArrow,DownArrow]);
    if Start < 1 then Start := 1;
  end;
  Glider( EntryRow+8, EntryCol+69, 10, 1, 0);
end;


procedure Edit(Num: integer);
var
  i,
  Start2,
  Current: byte;
  s: string;
begin
  Current := 1;
  BlankEntries;
  CDList[Num]^.Start  := 1;
  Draw( Num);
  WriteStatusLine(2);
  with CDList[Num]^ do
  begin
    Start2 := 0;
    repeat
      case Current of
        1: begin
             s := Artist;
             InputStr( s, EntryRow+4, EntryCol+6, SizeOf(Artist)-1, EntryA4, [Return, Escape,
               UpArrow, DownArrow, F8, AltC]);
             FillAt( EntryRow+4, EntryCol+6, 1, SizeOf(Artist)-1, EntryA3);
             Artist := s;
           end;
        2: begin
             s := Company;
             InputStr( s, EntryRow+4, EntryCol+39, SizeOf(Company)-1, EntryA4, [Return, Escape,
               UpArrow, DownArrow, F8, AltC]);
             FillAt( EntryRow+4, EntryCol+39, 1, SizeOf(Company)-1, EntryA3);
             Company := s;
           end;
        3: begin
             s := Year;
             InputStr( s, EntryRow+6, EntryCol+6, SizeOf(Year)-1, EntryA4, [Return, Escape,
               UpArrow, DownArrow, F8, AltC]);
             FillAt( EntryRow+6, EntryCol+6, 1, SizeOf(Year)-1, EntryA3);
             Year := s;
           end;
        4: begin
             s := Title;
             InputStr( s, EntryRow+6, EntryCol+13, SizeOf(Title)-1, EntryA4, [Return, Escape,
               UpArrow, DownArrow, F8, AltC]);
             FillAt( EntryRow+6, EntryCol+13, 1, SizeOf(Title)-1, EntryA3);
             Title := s;
           end;
        5..44: begin
             Glider( EntryRow+8, EntryCol+69, 10, Current-4, 40);
             if Current mod 2 = 1 then
             begin
               s := Song[(Current-3) div 2];
               InputStr( s, EntryRow+7+((Current-Start2-3) div 2), EntryCol+9,
                 SizeOf(Song[1])-1, EntryA4, [Return, Escape, UpArrow, DownArrow, F8, AltC]);
               Song[(Current-3) div 2] := s;
             end
             else begin
               s := Time[(Current-4) div 2];
               InputStr( s, EntryRow+7+((Current-Start2-3) div 2), EntryCol+62,
                 SizeOf(Time[1])-1, EntryA4, [Return, Escape, UpArrow, DownArrow, F8, AltC]);
               if Length( s) = 4 then s := '0' + s;
               i := Pos( '.', s);
               if i > 0 then
                 s[i] := ':';
               Time[(Current-3) div 2] := s;
             end;
             WriteSong( Num, (Current-Start2-3) div 2, (Current-3) div 2);
           end;
      end;
      case Key of
        UpArrow  : if Current > 1 then Dec(Current);
        Return,
        DownArrow: if Current < 44 then Inc(Current);
        AltC     : begin
                     Init( Num);
                     Draw( Num);
                   end;
      end;
      if (Current-Start2) > 24 then
      begin
        ScrollUp( EntryRow+8, EntryCol+5, 10, 55, EntryA3);
        ScrollUp( EntryRow+8, EntryCol+62, 10, 5, EntryA3);
        Inc(Start2,2);
        Inc(Start);
        WriteSong( Num, (Current-Start2-3) div 2, (Current-3) div 2);
      end;
      if ((Current-Start2) < 5) and (Current > 4) then
      begin
        ScrollDown( EntryRow+8, EntryCol+5, 10, 55, EntryA3);
        ScrollDown( EntryRow+8, EntryCol+62, 10, 5, EntryA3);
        Dec(Start2,2);
        Dec(Start);
        WriteSong( Num, (Current-Start2-3) div 2, (Current-3) div 2);
      end;
    until Key in [Escape,F8];
  end;
  Glider( EntryRow+8, EntryCol+69, 10, 1, 0);
  WriteStatusLine(1);
end;


function FileExists(Filename: string): boolean;
begin
  {$I-}
  Assign( CDFile, FileName);
  Reset( CDFile);
  Close( CDFile);
  {$I+}
  FileExists := (IOResult=0) and (Filename<>'');
end;


procedure ReadFromFile(Filename: string);
var
  i: integer;
begin
  NumCD := 0;
  {$I-}
  Assign( CDFile, FileName);
  Reset( CDFile);
  {$I+}
  if (IOResult<>0) or (Filename='') then
  begin
    MessageBox( 'No file named '+Filename);
    Exit;
  end;

  NumCD := FileSize( CDFile);
  if NumCD = 0 then
  begin
    MessageBox( 'Error loading file');
    Close( CDFile);
    Exit;
  end;

  for i := 1 to NumCD do
  begin
    CDList[i] := New( PCDEntry);
    Read( CDFile, CDList[i]^);
    CDList[i]^.Start := 1;
  end;
  Close( CDFile);
end;


procedure SaveToFile(Filename: string; var Changed: boolean);
var
  i: integer;
begin
  Assign( CDFile, FileName);
  Rewrite( CDFile);
  if NumCD > 0 then
  for i := 1 to NumCD do
    Write( CDFile, CDList[i]^);
  Close( CDFile);
  Changed := false;
end;


procedure AskFilename(var Filename: string);
var
  p: pointer;
begin
  GetMem( p, 2*7*52);
  StoreToMem( 10, 15, 7, 52, p^);
  Box( 10, 15, 6, 50, White+BlueBG, SingleBorder, ' ');
  AddShadow( 10, 15, 6, 50);
  Fill( 11, 16, 1, 48, Blue+WhiteBG, ' ');
  WriteC( 11, 40, SameAttr, 'Save');
  WriteStr( 13, 18, SameAttr, 'Filename:');
  InputStr( Filename, 13, 28, 32, Yellow+LightGrayBG, [Return,Escape]);
  StoreToScr( 10, 15, 7, 52, p^);
  FreeMem( p, 2*7*52);
end;


procedure SortList(var EntryNum: integer);
var
  i,j: integer;
  b: boolean;
  t: PCDEntry;
begin
  b := true;
  if NumCD > 1 then
  repeat
    b := true;
    for i := 1 to NumCD-1 do
    if UpcaseStr(CDList[i]^.Artist+CDList[i]^.Year) > UpcaseStr(CDList[i+1]^.Artist+CDList[i+1]^.Year) then
    begin
      t := CDList[i];
      CDList[i] := CDList[i+1];
      CDList[i+1] := t;
      b := false;
      if EntryNum = i then EntryNum := i+1
      else if EntryNum = i+1 then EntryNum := i;
    end;
  until b;
end;


procedure DeleteFromList(EntryNum: integer);
var
  i: integer;
begin
  if EntryNum = NumCD then
    Dispose( CDList[NumCD])
  else
  begin
    OneEntry := CDList[EntryNum];
    for i := EntryNum to NumCD-1 do
      CDList[i] := CDList[i+1];
    CDList[NumCD] := OneEntry;
    Dispose( CDList[NumCD])
  end;
  Dec( NumCD);
end;


procedure Index(var EntryNum: integer);
const
  Row= 4;
  Col= 20;
var
  s,i: integer;
  p: pointer;

  procedure WriteName( Row, i: integer);
  begin
    with CDList[i]^ do
    begin
      WriteStr( Row, Col+2, SameAttr, Artist);
      WriteStr( Row, Col+33, SameAttr, '('+Year+')');
    end;
  end;

  procedure WritePage( Start: integer);
  var
    i: byte;
  begin
    Fill( Row+3, Col+1, 15, 39, White+MagentaBG,' ');
    for i := 1 to 15 do
      if Start+i-1 <= NumCD then
        WriteName( Row+i+2, Start+i-1);
  end;

begin
  WriteStatusLine(3);
  GetMem( p, 2*20*44);
  StoreToMem( Row, Col, 20, 44, p^);
  Box( Row, Col, 19, 42, White+MagentaBG, SingleBorder, ' ');
  AddShadow( Row, Col, 19, 42);
  Fill( Row+1, Col+1, 1, 40, Magenta+WhiteBG,' ');
  WriteStr( Row+1, Col+20, SameAttr, 'Index');
  i := EntryNum;
  s := EntryNum;
  WritePage(i);
  repeat
    if NumCD > 1 then
      Glider( Row+2, Col+40, 16, i-1, NumCD-1);
    FillAt(Row+i-s+3, Col+1, 1, 39, Yellow+BlackBG);
    InKey( Ch, Key);
    FillAt(Row+i-s+3, Col+1, 1, 39, White+MagentaBG);
    case Key of
      UpArrow  : if i > 1 then
                 begin
                   Dec(i);
                   if i < s then
                   begin
                     Dec(s);
                     ScrollDown(Row+3, Col+1, 15, 39, White+MagentaBG);
                     WriteName( Row+3, i);
                   end;
                 end;
      DownArrow: if i < NumCD then
                 begin
                   Inc(i);
                   if i > (s+14) then
                   begin
                     Inc(s);
                     ScrollUp(Row+3, Col+1, 15, 39, White+MagentaBG);
                     WriteName( Row+17, i);
                   end;
                 end;
      PgUp     : if i > 15 then
                 begin
                   Dec(i,15);
                   Dec(s,15);
                   if s < 1 then s := 1;
                   WritePage(s);
                 end;
      PgDn     : if (s+15) <= NumCD then
                 begin
                   Inc(i,15);
                   if i > NumCD then i := NumCD;
                   Inc(s,15);
                   WritePage(s);
                 end;
      HomeKey  : if i > 1 then
                 begin
                   i := 1;
                   s := 1;
                   WritePage(s);
                 end;
      EndKey  :  if i < NumCD then
                 begin
                   i := NumCD;
                   s := i - 14;
                   if s < 1 then s := 1;
                   WritePage(s);
                 end;
    end;
  until Key in [Escape, Return];
  if Key=Return then
    EntryNum := i;
  Key := NullKey;
  StoreToScr( Row, Col, 20, 44, p^);
  FreeMem( p, 2*20*44);
  WriteStatusLine(1);
end;


procedure FindLetter(c: char;  var EntryNum: integer);
var
  i: integer;
begin
  i := 0;
  case c of
    '�' : c := '�';
    '�' : c := '�';
    '�' : c := '�';
  else
    c := Upcase(c);
  end;
  if NumCD <= 0 then Exit;
  repeat
    Inc(i);
  until (CDList[i]^.Artist[1]>=c) or (i>=NumCD);
  EntryNum := i;
end;


function Search(var SearchString: string;  var EntryNum: integer;  Again: boolean): boolean;
var
  i    : integer;
  j,k,l: byte;
  Found: boolean;

  procedure AskString(var SearchString: string);
  var
    p: pointer;
    s: string;
  begin
    s := SearchString;
    GetMem( p, 2*7*52);
    StoreToMem( 10, 15, 7, 52, p^);
    Box( 10, 15, 6, 50, White+BlueBG, SingleBorder, ' ');
    AddShadow( 10, 15, 6, 50);
    Fill( 11, 16, 1, 48, Blue+WhiteBG, ' ');
    WriteC( 11, 40, SameAttr, 'Search String');
    WriteStr( 13, 18, SameAttr, 'String:');
    InputStr( s, 13, 28, 32, Yellow+LightGrayBG, [Return,Escape]);
    StoreToScr( 10, 15, 7, 52, p^);
    FreeMem( p, 2*7*52);
    if Key = Return then
      SearchString := s;
  end;

begin
  if Again then
    i := EntryNum
  else begin
    i := 0;
    AskString( SearchString);
    if Key = Escape then
    begin
      Key := NullKey;
      Exit;
    end;
  end;
  if i = NumCD then
    Exit;
  SearchString := UpcaseStr( SearchString);
  l := Length( SearchString);
  Found := false;
  repeat
    Inc(i);
    with CDList[i]^ do
    begin
      if Pos(SearchString, UpcaseStr(Artist)) > 0 then Found := true;
      if Pos(SearchString, UpcaseStr(Company)) > 0 then Found := true;
      if Pos(SearchString, UpcaseStr(Year)) > 0 then Found := true;
      if Pos(SearchString, UpcaseStr(Title)) > 0 then Found := true;
      if not Found then
      for j := 1 to MaxSongs do
      if Pos(SearchString, UpcaseStr(Song[j])) > 0 then
      begin
        Found := true;
        Break;
      end;
    end;
  until Found or (i >= NumCD);
  if Found then
    EntryNum := i;
  Search := Found;
end;


procedure Help;
const
  HRow = 5;
  HCol = 20;
  HRows= 19;
  HCols= 40;
var
  p: pointer;
begin
  GetMem(p, 2*HRows*HCols);
  StoreToMem( HRow, HCol, HRows, HCols, p^);

  Box( HRow, HCol, HRows-1, HCols-2, White+GreenBG, SingleBorder, ' ');
  AddShadow( HRow, HCol, HRows-1, HCols-2);
  Fill(HRow+1, HCol+1, 1, HCols-4, Green+WhiteBG, ' ');
  WriteC(HRow+1, HCol+(HCols div 2)-2, SameAttr, 'Help');
  WriteStr( HRow+3, HCol+3, LightCyan+GreenBG, 'F1    ');
    WriteEos( SameAttr, '- This help');
  WriteStr( HRow+4, HCol+3, LightCyan+GreenBG, 'F2    ');
    WriteEos( SameAttr, '- Save library to file');
  WriteStr( HRow+5, HCol+3, LightCyan+GreenBG, 'F3    ');
    WriteEos( SameAttr, '- Load library from file');
  WriteStr( HRow+6, HCol+3, LightCyan+GreenBG, 'F8    ');
    WriteEos( SameAttr, '- Edit current entry');
  WriteStr( HRow+7, HCol+3, LightCyan+GreenBG, #27+#26+'    ');
    WriteEos( SameAttr, '- Select entry');
  WriteStr( HRow+8, HCol+3, LightCyan+GreenBG, 'Alt-I ');
    WriteEos( SameAttr, '- Show artist index');
  WriteStr( HRow+9, HCol+3, LightCyan+GreenBG, 'Alt-S ');
    WriteEos( SameAttr, '- Search for string');
  WriteStr( HRow+10, HCol+3, LightCyan+GreenBG, 'Alt-N ');
    WriteEos( SameAttr, '- Find next occurence');
  WriteStr( HRow+11, HCol+3, LightCyan+GreenBG, 'Ins   ');
    WriteEos( SameAttr, '- Insert and edit new entry');
  WriteStr( HRow+12, HCol+3, LightCyan+GreenBG, 'Del   ');
    WriteEos( SameAttr, '- Delete current entry');
  WriteStr( HRow+13, HCol+3, LightCyan+GreenBG, 'Esc   ');
    WriteEos( SameAttr, '- Quit CDLibrary');
  PushButton( HRow+HRows-4, HCol+(HCols div 2)-4, Green+WhiteBG, White+GreenBG, #16+' Ok '+#17, false);
  Key := NullKey;

  StoreToScr( HRow, HCol, HRows, HCols, p^);
  FreeMem(p, 2*HRows*HCols);
end;


procedure MainMenu;
var
  EntryNum  : integer;
  SearchString,
  s,OldName : string;
  Changed,
  Scrolled  : boolean;
begin
  Fill( 1, 1, CRTRows, 80, White+BlueBG, '�');
  WriteStatusLine(1);
  MainBackground;
  OneEntry := New( PCDEntry);
  DrawLetters('A');
  NumCD := 0;
  EntryNum := 0;
  Scrolled := false;
  Changed := false;
  SearchString := '';
  Key := NullKey;
  s := 'Record '+StrL(EntryNum)+':'+StrL(NumCD);
  WriteStr( CRTRows, 80-Length(s), SameAttr, s);
  repeat
    if not Scrolled then
      Inkey( Ch, Key)
    else
      Scrolled := false;
    case Key of
      UpArrow,
      DownArrow: if NumCD > 0 then
              begin
                Scroll( EntryNum);
                Scrolled := true;
              end;
      AltI  : if NumCD > 0 then
              begin
                Index( EntryNum);
                Draw( EntryNum);
              end;
      AltS  : if NumCD > 0 then
              begin
                if Search(SearchString, EntryNum, false) then
                  Draw( EntryNum)
                else
                  MessageBox('String not found');
                Key := NullKey;
              end;
      AltN  : if NumCD > 0 then
              begin
                if Search(SearchString, EntryNum, true) then
                  Draw( EntryNum)
                else
                  MessageBox('String not found');
                Key := NullKey;
              end;
      InsKey: begin
                Inc( NumCD);
                CDList[NumCD] := New( PCDEntry);
                if NumCD > 1 then
                  CDList[NumCD]^ := CDList[EntryNum]^
                else Init( NumCD);
                Edit( NumCD);
                if Key=Escape then
                begin
                  Dispose( CDList[NumCD]);
                  Dec( NumCD);
                end
                else
                begin
                  EntryNum := NumCD;
                  SortList( EntryNum);
                  Changed := true;
                end;
                if EntryNum > 0 then
                  Draw( EntryNum);
                Key := NullKey;
              end;
      DelKey: if Confirm ('Delete record '+CDList[EntryNum]^.Artist,false) then
              begin
                DeleteFromList( EntryNum);
                if EntryNum > NumCD then
                  EntryNum := NumCD;
                Draw( EntryNum);
                Changed := true;
              end;
      F1    : Help;
      F2    : if Changed and (NumCD > 0) then
              begin
                AskFilename( Filename);
                if Key=Return then
                begin
                  if FileExists( Filename) then
                  begin
                    if Confirm('File exists!  Overwrite '+Filename, false) then
                      SaveToFile( Filename, Changed);
                  end
                  else SaveToFile( Filename, Changed);
                end;
                MainBackGround;
                Draw( EntryNum);
                Key := NullKey;
              end;
      F3    : begin
                if Changed then
                  if Confirm('File changed. Save first', true) then
                    SaveToFile( Filename, Changed);
                OldName := Filename;
                OpenFile( 4, 15, Filename);
                if Filename <> OldName then
                begin
                  ReadFromFile( Filename);
                  EntryNum := 0;
                  BlankEntries;
                  if NumCD > 0 then
                  begin
                    EntryNum := 1;
                    MainBackGround;
                    Draw( EntryNum);
                  end;
                end;
                Key := NullKey;
              end;
      F8    : if NumCD > 0 then
              begin
                OneEntry^ := CDList[EntryNum]^;
                Edit( EntryNum);
                if Key=F8 then
                begin
                  SortList( EntryNum);
                  Changed := true;
                end
                else
                  CDList[EntryNum]^ := OneEntry^;
                Draw( EntryNum);
                Key := NullKey;
              end;
      TextKey:if NumCD > 0 then
              begin
                FindLetter( Ch, EntryNum);
                Draw( EntryNum);
                Key := NullKey;
              end;
      RightArrow: if EntryNum < NumCD then
              begin
                Inc( EntryNum);
                Draw( EntryNum);
                Key := NullKey;
              end;
      LeftArrow: if EntryNum > 1 then
              begin
                Dec( EntryNum);
                Draw( EntryNum);
                Key := NullKey;
              end
              else Key := NullKey;
    end;
    Glider( EntryRow+8, EntryCol+69, 10, CDList[EntryNum]^.Start-1, 11);
    Fill( CRTRows, 60, 1, 20, White+CyanBG, ' ');
    s := 'Record '+StrL(EntryNum)+':'+StrL(NumCD);
    WriteStr( CRTRows, 80-Length(s), SameAttr, s);
  until Key=Escape;
  if Changed then
    if Confirm('File has changed. Save',true) then
      SaveToFile( Filename, Changed);
  for EntryNum := 1 to NumCD do
    Dispose( CDList[EntryNum]);
  ClrScr;
end;


begin
  GetDir( 0, CurrentPath);
  if Length( CurrentPath) > 3 then
    CurrentPath := CurrentPath + '\';
  SearchPath := '*.REC';
  Filename := '';
  SetIntens;
  SetCursor( CursorOff);

  MainMenu;

  SetBlink;
  SetCursor( CursorUnderline);
end.