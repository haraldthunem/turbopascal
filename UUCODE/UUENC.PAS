program UU_Enc;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� INFO 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{� File    : UUENC.PAS                                                      �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : UUcoding program                                               �}
{� Updated : August 15 1994                                                 �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$A+   Word align data                                                       }
{$B-   Short-circuit Boolean expression evaluation                           }
{$E-   Disable linking with 8087-emulating run-time library                  }
{$G+   Enable 80286 code generation                                          }
{$R-   Disable generation of range-checking code                             }
{$S-   Disable generation of stack-overflow checking code                    }
{$V-   String variable checking                                              }
{$X-   Disable Turbo Pascal's extended syntax                                }
{$N-   80x87 code generation                                                 }
{$D-   Disable generation of debug information                               }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

uses
  Crt;

const
  LineConst       = 100;
  CharsPerLine    = 60;
  BytesPerLine    = 45;
  CharBufSize     = CharsPerLine * LineConst;
  ByteBufSize     = BytesPerLine * LineConst;

  Header          = 'begin';
  Trailer         = 'end';
  DefaultMode     = '644';
  DefaultExtension= '.uue';
  Offset          = 32;
  SixBitMask      = $3F;

var
  CharBuffer      : array[1..CharBufSize] of char;
  ByteBuffer      : array[1..ByteBufSize] of byte;
  InOk,
  OutOk           : boolean;
  InFile          : file;
  OutFile         : text;
  InFileName,
  OutFileName,
  Mode            : string;
  NumRead,
  NumWrite        : word;
  Bytes           : array[1..3] of byte;
  Chars           : array[1..4] of byte;


procedure Abort( Msg: string);
begin { Abort }
  WriteLn( Msg);
  if InOk then Close(Infile);
  if OutOk then Close(OutFile);
  Halt( 1);
end; { Abort }


procedure Terminate;
begin { Terminate }
  WriteLn( OutFile, Trailer);
  if InOk then Close(Infile);
  if OutOk then Close(OutFile);
end; { Terminate }


procedure Init;
var
  i   : integer;
  Temp: string;
  Ch  : char;
begin { Init }
  InOk  := false;
  OutOk := false;
  if ParamCount < 1 then
    Abort( 'No input file specified.');
  InFileName := ParamStr(1);
  {$I-}
  Assign( InFile, InFileName);
  Reset( InFile, 1);
  {$i+}
  if IOResult > 0 then
    Abort( 'Can''t open file ' + InFileName + '.');
  InOk := true;

  i := Pos( '.', InFileName);
  if i = 0 then
    OutFileName := InFileName
  else
    OutFileName := Copy( InFileName, 1, i-1);

  Mode := DefaultMode;
  if ParamCount > 1 then
    for i := 2 to ParamCount do
    begin
      Temp := Paramstr( i);
      if Temp[1] in ['0'..'9'] then
        Mode := Temp
      else
        OutFileName := Temp;
    end;
  if Pos( '.', OutFileName) = 0 then
    OutFileName := OutFileName + DefaultExtension;
  Assign( OutFile, OutFileName);
  WriteLn( 'Uuencoding file ' + InFileName + ' to file ' + OutFileName + '.');

  {$i-}
  Reset(OutFile);
  {$i+}
  if IOResult = 0 then
  begin
    Write( 'Overwrite current ', OutFileName, '? [Y/N] ');
    repeat
      Ch := Upcase( ReadKey);
    until Ch in ['Y', 'N'];
    WriteLn( Ch);
    if Ch = 'N' then
      Abort( OutFileName + ' not overwritten.');
  end;

  {$i-}
  Rewrite(OutFile);
  {$i+}
  if IOResult > 0 then
    Abort( 'Can''t open ' + OutFileName);

  OutOk := true;
  WriteLn( OutFile, 'UU-Encoder 2.0  written by Harald Thunem');
  WriteLn( OutFile, Header, ' ', Mode, ' ', InFileName);
end; { Init }


procedure EncodeBuffer( NumRead: word);
var
  i,j: word;
begin
  i := 0;
  j := 0;
{  while NumRead mod 3 <> 0 do Inc( NumRead);}
  repeat
    Bytes[1] := ByteBuffer[i+1];
    Bytes[2] := ByteBuffer[i+2];
    Bytes[3] := ByteBuffer[i+3];
    Chars[1] :=  Bytes[1] shr 2;
    Chars[2] := (Bytes[1] shl 4) + (Bytes[2] shr 4);
    Chars[3] := (Bytes[2] shl 2) + (Bytes[3] shr 6);
    Chars[4] :=  Bytes[3];
    CharBuffer[j+1] := Chr( (Chars[1] and SixBitMask) + Offset);
    CharBuffer[j+2] := Chr( (Chars[2] and SixBitMask) + Offset);
    CharBuffer[j+3] := Chr( (Chars[3] and SixBitMask) + Offset);
    CharBuffer[j+4] := Chr( (Chars[4] and SixBitMask) + Offset);
    Inc( i, 3);
    Inc( j, 4);
  until (i>=NumRead);
  NumWrite := (4 * NumRead) div 3;
end;


procedure WriteBuffer( NumWrite: word);
var
  i,j,Num1,Num2: word;

  procedure WriteOut( Ch: char);
  begin
    if Ch = ' ' then Write( OutFile, '`')
                else Write( OutFile, Ch)
  end;

begin { WriteBuffer }
  i := 0;
  Num2 := NumRead;
  repeat
    if (NumWrite-i) >= CharsPerLine then
    begin
      WriteOut( Chr( BytesPerLine + Offset));
      for j := 1 to CharsPerLine do
        WriteOut( CharBuffer[i+j]);
      Inc( i, CharsPerLine);
      Dec( Num2, BytesPerLine);
    end
    else begin
      Num1 := NumWrite-i;
      WriteOut( Chr( Num2 + Offset));
      for j := 1 to Num1 do
        WriteOut( CharBuffer[i+j]);
      Inc( i, Num1);
    end;
    WriteLn( OutFile);
  until (i >= NumWrite);
end; { WriteBuffer }


procedure Encode;
begin { Encode }
  repeat
    Write('.');
    FillChar( ByteBuffer, ByteBufSize, $00);
    FillChar( CharBuffer, CharBufSize, Chr( 0));
    BlockRead( InFile, ByteBuffer, ByteBufSize, NumRead);
    EncodeBuffer( NumRead);
    WriteBuffer( NumWrite);
  until (NumRead <> ByteBufSize);
  WriteLn( OutFile, '`');
end; { Encode }


begin { UU_Dos }
  Init;
  Encode;
  Terminate;
end. { UU_Dos }
