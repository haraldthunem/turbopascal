program LoadScreen;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴� INFO 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{� File    : LOADSCR.PAS                                                    �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : Load a VGA text screen image from file.                        �}
{� Updated : July 13 1992                                                   �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$A+   Word align data                                                       }
{$B-   Short-circuit Boolean expression evaluation                           }
{$E-   Disable linking with 8087-emulating run-time library                  }
{$G+   Enable 80286 code generation                                          }
{$R-   Disable generation of range-checking code                             }
{$S-   Disable generation of stack-overflow checking code                    }
{$V-   String variable checking                                              }
{$X-   Disable Turbo Pascal's extended syntax                                }
{$N+   80x87 code generation                                                 }
{$D-   Disable generation of debug information                               }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

uses
  Dos,      { TP DOS routines    }
  UStrs,    { HT String routines }
  UScreen;  { HT Screen routines }

var
  Path : PathStr;
  Dir  : DirStr;
  Name : NameStr;
  Ext  : ExtStr;


{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴 Main Program 컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
begin
  WriteLn('Load Screen v2.0');
  WriteLn;
  if ParamCount<>1 then
  begin
    WriteLn('Wrong number of parameters.');
    WriteLn('Usage : LOADSCR filename[.ext]');
    Halt(1);
  end;
  Path := UpcaseStr(ParamStr(1));
  FSplit(Path,Dir,Name,Ext);
  if Ext='' then
    Path := Dir+Name+'.SCR';
  if LoadScreenFromFile(Path) then
  else WriteLn('Error loading screenfile ',Path);
end. { LoadScreen }
