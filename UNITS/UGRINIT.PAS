unit UGrInit;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� File    : UGrINIT.PAS                                                    �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : Routines to initialize TP graphics                             �}
{� Updated : October 28 1993                                                �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� procedure Initialize;                                                    �}
{� Purpose:  Initializes the graphics                                       �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$O+   Overlay code generation                                               }
{$F+   Force far calls                                                       }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
interface
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}


uses
{  Crt,   { TP screen routines   }
  Graph; { TP graphics routines }

var
  Xasp,Yasp    : word;
  Palett       : PaletteType;
  OldExitProc  : pointer;
  GraphDriver  : integer;
  GraphMode    : integer;
  MaxX,MaxY    : word;
  MaxColor     : word;
  ErrorCode    : integer;


procedure Initialize;


{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
implementation
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

procedure Initialize;
var
  InGraphicsMode : boolean; { Flags initialization of graphics mode }
  PathToDriver   : string;  { Stores the DOS path to *.BGI & *.CHR }
  Chosen         : integer;
begin
  { when using Crt and graphics, turn off Crt's memory-mapped writes }
{  DirectVideo := False;}
  PathToDriver := 'C:\BP\BGI';
  repeat
    GraphDriver := Detect;
    InitGraph(GraphDriver, GraphMode, PathToDriver);
    ErrorCode := GraphResult;             { preserve error return }
    if ErrorCode <> grOK then             { error? }
    begin
      WriteLn('Graphical error : ', GraphErrorMsg(ErrorCode));
      if ErrorCode = grFileNotFound then  { Can't find driver file }
      begin
        WriteLn('Write directory with BGI-files : ');
        ReadLn(PathToDriver);
        WriteLn;
      end
      else Halt( 1);
    end;
  until ErrorCode = grOK;
  Randomize;                { init random number generator }
  MaxColor := GetMaxColor;  { Get the maximum allowable drawing color }
  MaxX := GetMaxX;          { Get screen resolution values }
  MaxY := GetMaxY;
end; { Initialize }

end. { UGrInit }

