unit UNCommon;
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� File    : UNCOMMON.PAS                                                   �}
{� Author  : Harald Thunem                                                  �}
{� Purpose : Same as UCommon, except uses UNBorder in boxes.                �}
{� Updated : October 28 1993                                                �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� procedure PushButton(R,C,Attr,BackAttr: byte; s: string;                 �}
{�                      Pushed: boolean);                                   �}
{� Purpose:  Provides a push button. Press Return or Escape to activate.    �}
{�           Returns the key that was pushed.                               �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� function  Confirm(Msg: string; Select: boolean): boolean;                �}
{� Purpose:  Displays a confirm box with the choice between Yes and No.     �}
{�           Returns true if pressed Yes.                                   �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� procedure MessageBox(Msg: string);                                       �}
{� Purpose:  Displays a message box.                                        �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}
{旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커}
{� procedure OpenFile(OpenRow,OpenCol: byte;  var Filename: string);        �}
{� Purpose:  Provides an Open file dialog box. Set the global variables     �}
{�           SearchPath and CurrentPath to specify search criteria.         �}
{�           Ex. To search for all .PAS file in the C:\USER directory, set  �}
{�           SearchPath to '*.PAS' and CurrentPath to 'C:\USER\'.           �}
{�           While the box is open, press F3 to change SearchPath.          �}
{읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸}

{컴컴컴컴컴컴컴컴컴컴컴컴컴 Compiler directives 컴컴컴컴컴컴컴컴컴컴컴컴컴컴�}
{$O+   Overlay code generation                                               }
{$F+   Force far calls                                                       }
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
interface
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

var
  SearchPath,
  CurrentPath: string;


procedure PushButton(R,C,Attr,BackAttr: byte; s: string; Pushed: boolean);
function Confirm(Msg: string; Select: boolean): boolean;
procedure MessageBox(Msg: string);
procedure OpenFile(OpenRow,OpenCol: byte;  var Filename: string);


{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}
implementation
{컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴}

uses
  Dos,      { TP DOS routines        }
  UScreen,  { HT Screen routines     }
  UNBorder, { HT New border routines }
  UKeys;    { HT Keyboard routines   }


procedure PushButton;
var
  a,l: byte;

  procedure Shadow(R,C,L: byte);
  begin
    Fill(R+1,C+1,1,L,a,'�');
    WriteStr(R,C+L,a,'�');
  end; { Shadow }

begin
  a := BackAttr and $F0;
  l := Length(s);
  Fill(R,C,2,l+1,BackAttr,' ');
  WriteStr(R,C,Attr,s);
  Shadow(R,C,Length(s));
  if not Pushed then
  repeat
    InKey(Ch,Key);
  until Key in [Return,Escape];
  Fill(R,C,2,l+1,BackAttr,' ');
  WriteStr(R,C+1,Attr,s);
  Delay(100);
  Fill(R,C,2,l+1,BackAttr,' ');
  WriteStr(R,C,Attr,s);
  Shadow(R,C,Length(s));
  Delay(100);
end; { PushButton }


function Confirm;
const
  MessageAttr = White+RedBG;
  TopAttr     = Green+WhiteBG;
var
  L,Row       : byte;
  Size        : integer;
  Scr         : pointer;
begin
  if Pos('?',Msg)<=0 then Msg := Msg + ' ?';
  Row := (CRTRows div 2)-3;
  L := 5+(Length(Msg) div 2);
  if L<10 then L:=10;
  Size := 2*9*60;
  GetMem(Scr,Size);
  StoreToMem(Row,8,9,60,Scr^);
  NewBox(Row,40-L,8,2*L,MessageAttr,' ');
  AddShadow(Row,40-L,8,2*L);
  Fill(Row,40-L,1,2*L,TopAttr,' ');
  WriteC(Row,40,TopAttr,'Confirm');
  WriteC(Row+3,40,MessageAttr,Msg);
  if Select then
    WriteStr(Row+5,32,Blue+WhiteBG,#16+' Yes '+#17)
  else WriteStr(Row+5,32,Blue+LightGrayBG,'  Yes  ');
  WriteStr(Row+6,33,Black+RedBG,'賽賽賽�');
  WriteStr(Row+5,39,Black+RedBG,'�');
  if Select then
    WriteStr(Row+5,41,Blue+LightGrayBG,'  No   ')
  else WriteStr(Row+5,41,Blue+WhiteBG,#16+' No  '+#17);
  WriteStr(Row+6,42,Black+RedBG,'賽賽賽�');
  WriteStr(Row+5,48,Black+RedBG,'�');
  repeat
    InKey(Ch,Key);
    Ch := Upcase(Ch);
    WriteStr(Row+5,32,Blue+LightGrayBG,'  Yes  ');
    WriteStr(Row+5,41,Blue+LightGrayBG,'  No   ');
    if Key in [LeftArrow,RightArrow] then
      Select := not Select;
    if Select then
      WriteStr(Row+5,32,Blue+WhiteBG,#16+' Yes '+#17)
    else WriteStr(Row+5,41,Blue+WhiteBG,#16+' No  '+#17);
  until (Ch in ['Y','N']) or (Key in [Return,Escape]);
  WriteStr(Row+5,32,Blue+LightGrayBG,'  Yes  ');
  WriteStr(Row+5,41,Blue+LightGrayBG,'  No   ');
  if (Ch='Y') then Select := true;
  if (Ch='N') then Select := false;
  if Key=Escape then Select := false;
  if Select then PushButton(Row+5,32,Blue+WhiteBG,MessageAttr,#16+' Yes '+#17,true)
    else PushButton(Row+5,41,Blue+WhiteBG,MessageAttr,#16+' No  '+#17,true);

  Confirm := Select;
  StoreToScr(Row,8,9,60,Scr^);
  Freemem(Scr,Size);
  Key := NullKey;
end; { Confirm }


procedure MessageBox;
const
  MessageAttr = White+RedBG;
  TopAttr     = Green+WhiteBG;
var
  L,Row       : byte;
  Size        : integer;
  Scr         : pointer;
begin
  Row := (CRTRows div 2)-3;
  L := 4+(Length(Msg) div 2);
  Size := 2*9*60;
  GetMem(Scr,Size);
  StoreToMem(Row,8,9,60,Scr^);
  NewBox(Row,40-L,8,2*L,MessageAttr,' ');
  AddShadow(Row,40-L,8,2*L);
  Fill(Row,40-L,1,2*L,TopAttr,' ');
  WriteC(Row,40,TopAttr,'Message');
  WriteC(Row+3,40,MessageAttr,Msg);
  PushButton(Row+5,37,Blue+WhiteBG,MessageAttr,#16+' OK '+#17,false);
  StoreToScr(Row,8,9,60,Scr^);
  Freemem(Scr,Size);
end; { MessageBox }


procedure OpenFile;
const
  OpenAttr = White+LightGrayBG;
  OpenAttr2= White+CyanBG;
  DirAttr  = LightCyan+LightGrayBG;
  TopAttr  = Green+WhiteBG;
  SlideAttr= White+GreenBG;
  HighAttr = Yellow+MagentaBG;
  MaxFiles = 1000;

type
  FileType = record
               Attr : Byte;
               Time : Longint;
               Size : Longint;
               Name : string[12];
             end;
  PFile    = ^FileType;

var
  FileList : array[1..MaxFiles] of PFile;
  NumFiles : integer;
  ImSize,
  Size: integer;
  Scr : pointer;

  procedure ScanForFiles(CurrentPath,SearchPath: string);
  var
    S: SearchRec;
  begin
    NumFiles := 0;
    FindFirst(CurrentPath+'*.*',AnyFile,S);
    while DosError=0 do
    begin
      if (S.Name<>'.') and (S.Attr=Directory) then
      begin
        Inc(NumFiles);
        GetMem(FileList[NumFiles],Size);
        FileList[NumFiles]^.Attr := S.Attr;
        FileList[NumFiles]^.Time := S.Time;
        FileList[NumFiles]^.Size := S.Size;
        FileList[NumFiles]^.Name := S.Name;
      end;
      FindNext(S);
    end;
    FindFirst(CurrentPath+SearchPath,ReadOnly+Archive+Hidden,S);
    while DosError=0 do
    begin
      Inc(NumFiles);
      GetMem(FileList[NumFiles],Size);
      FileList[NumFiles]^.Attr := S.Attr;
      FileList[NumFiles]^.Time := S.Time;
      FileList[NumFiles]^.Size := S.Size;
      FileList[NumFiles]^.Name := S.Name;
      FindNext(S);
    end;
  end; { ScanForFiles }

  procedure SortFileList;
  var
    i: integer;
    b: boolean;
    t: PFile;
  begin
    repeat
      b := true;
      for i := 1 to NumFiles-1 do
      if FileList[i]^.Name > FileList[i+1]^.Name then
      begin
        t:=FileList[i]; FileList[i]:=FileList[i+1]; FileList[i+1]:=t; b:=False;
      end;
    until b;
    repeat
      b := true;
      for i := 1 to NumFiles-1 do
      if (FileList[i]^.Attr and Directory<>Directory) and (FileList[i+1]^.Attr and Directory=Directory) then
      begin
        t:=FileList[i]; FileList[i]:=FileList[i+1]; FileList[i+1]:=t; b:=False;
      end;
    until b;
  end; { SortFileList }

  procedure EraseFileList;
  var
    i: integer;
  begin
    for i := 1 to NumFiles do
      FreeMem(FileList[i],Size);
  end; { EraseFileList }

  procedure ClearOpen;
  var
    i: integer;
  begin
    Fill(OpenRow+1,OpenCol+1,10,42,OpenAttr,' ');
    for i := 1 to 10 do
    begin
      WriteStr(OpenRow+i,OpenCol+14,OpenAttr,'�');
      WriteStr(OpenRow+i,OpenCol+28,OpenAttr,'�');
    end;
  end; { ClearOpen }

  procedure DrawBackground;
  begin
    NewBox(OpenRow,OpenCol,18,44,OpenAttr,' ');
    AddShadow(OpenRow,OpenCol,18,44);
    NewBox(OpenRow+11,OpenCol,7,44,OpenAttr2,' ');
    Fill(OpenRow,OpenCol,1,44,TopAttr,' ');
    WriteC(OpenRow,OpenCol+20,TopAttr,'Open File');
    WriteStr(OpenRow+11,OpenCol,TopAttr,' ');
    WriteStr(OpenRow+11,OpenCol+43,TopAttr,' ');
    Fill(OpenRow+11,OpenCol+2,1,40,SlideAttr,'�');
    WriteStr(OpenRow+11,OpenCol+1,SlideAttr,#17);
    WriteStr(OpenRow+11,OpenCol+42,SlideAttr,#16);
    ClearOpen;
  end; { DrawBackground }

  procedure WriteFileList(StartNum: integer);
  var
    i,j: integer;
  begin
    ClearOpen;
    i := StartNum-1;
    repeat
      Inc(i);
      j := i-StartNum;
      if FileList[i]^.Attr=Directory then
        WriteStr(OpenRow+1+(j mod 10),OpenCol+2+14*(j div 10),DirAttr,FileList[i]^.Name)
      else WriteStr(OpenRow+1+(j mod 10),OpenCol+2+14*(j div 10),OpenAttr,FileList[i]^.Name);
    until (i-StartNum >= 29) or (i=NumFiles);
  end; { WriteFileList }

  procedure LightName(StartNum,i: integer;  b: boolean);
  var
    j: integer;
    a: byte;
    s: string[13];
  begin
    if b then a:=HighAttr
    else if FileList[i]^.Attr = Directory then a:=DirAttr
    else a := OpenAttr;
    j := i-StartNum;
    s := ' '+FileList[i]^.Name+'            ';
    WriteStr(OpenRow+1+(j mod 10),OpenCol+1+14*(j div 10),a,s);
  end; { LightName }

  procedure WriteInfo(i: integer);
  const
    DateStr : array[1..12] of string[3] = ('Jan','Feb','Mar','Apr','May','Jun',
                                           'Jul','Aug','Sep','Oct','Nov','Dec');
  var
    DT: DateTime;
    s,s1: string;
    a: byte;
  begin
    Fill(OpenRow+11,OpenCol+2,1,40,SlideAttr,'�');
    if NumFiles>1 then
      a := 2+Trunc(39*(i-1)/(NumFiles-1))
    else a:=2;
    WriteStr(OpenRow+11,OpenCol+a,SlideAttr,'�');
    WriteStr(OpenRow+13,OpenCol+2,OpenAttr2,'File :');
    WriteStr(OpenRow+14,OpenCol+2,OpenAttr2,'Size :');
    WriteStr(OpenRow+15,OpenCol+2,OpenAttr2,'Attr :');
    WriteStr(OpenRow+16,OpenCol+2,OpenAttr2,'Path :');
    WriteStr(OpenRow+13,OpenCol+23,OpenAttr2,'Time :');
    WriteStr(OpenRow+14,OpenCol+23,OpenAttr2,'Date :');
    s := Copy(FileList[i]^.Name+'            ',1,12);
    WriteStr(OpenRow+13,OpenCol+9,OpenAttr2,s);
    Str(FileList[i]^.Size:1,s);
    s := Copy(s+'            ',1,12);
    WriteStr(OpenRow+14,OpenCol+9,OpenAttr2,s);
    a := FileList[i]^.Attr;
    if (a and Directory)=Directory then WriteStr(OpenRow+15,OpenCol+9,OpenAttr2,   'Directory')
    else if (a and Archive)=Archive then WriteStr(OpenRow+15,OpenCol+9,OpenAttr2,  'Archive  ')
    else if (a and ReadOnly)=ReadOnly then WriteStr(OpenRow+15,OpenCol+9,OpenAttr2,'ReadOnly ')
    else if (a and Hidden)=Hidden then WriteStr(OpenRow+15,OpenCol+9,OpenAttr2,    'Hidden   ');
    s := CurrentPath+SearchPath;
    if Length(s)>34 then
      s := Copy(s,1,34);
    WriteStr(OpenRow+16,OpenCol+9,OpenAttr2,'                                  ');
    WriteStr(OpenRow+16,OpenCol+9,OpenAttr2,s);

    UnpackTime(FileList[i]^.Time,DT);
    s := '';
    Str(DT.Hour:1,s);
    if DT.Hour<10 then s := '0'+s;
    Str(DT.Min:1,s1);
    if DT.Min<10 then s1 := '0'+s1;
    s := s+':'+s1;
    Str(DT.Sec:1,s1);
    if DT.Sec<10 then s1 := '0'+s1;
    s := s+':'+s1;
    WriteStr(OpenRow+13,OpenCol+30,OpenAttr2,s);
    s := DateStr[DT.Month];
    Str(DT.Day:1,s1);
    if DT.Day<10 then s1 := '0'+s1;
    s := s+'.'+s1;
    Str(DT.Year:1,s1);
    s := s+' '+s1;
    WriteStr(OpenRow+14,OpenCol+30,OpenAttr2,s);
  end; { WriteInfo }

  procedure NewSearchPath;
  const
    NewAttr = White+RedBG;
    EditAttr= LightCyan+LightGrayBG;
  var
    s: string;
  begin
    NewBox(OpenRow+5,OpenCol+10,3,21,NewAttr,' ');
    AddShadow(OpenRow+5,OpenCol+10,3,21);
    WriteStr(OpenRow+6,OpenCol+12,NewAttr,'Path ');
    s := SearchPath;
    InputStr(s,OpenRow+6,OpenCol+17,12,EditAttr,[Escape,Return]);
    if Key=Return then
      SearchPath := s;
    Key := NullKey;
  end; { NewSearchPath }

  procedure SelectFile;
  var
    i,j,StartNum,
    OldStartNum: integer;
  begin
    StartNum := 1;
    OldStartNum := 1;
    i := 1;
    WriteFileList(StartNum);
    LightName(StartNum,i,true);
    WriteInfo(i);
    repeat
      InKey(Ch,Key);
      LightName(StartNum,i,false);
      case Key of
        UpArrow   : if i > 1 then Dec(i);
        DownArrow : if i < NumFiles then Inc(i);
        LeftArrow : if i > 10 then Dec(i,10) else i := 1;
        RightArrow: if i < NumFiles-10 then Inc(i,10) else i := NumFiles;
        F3        : begin
                      NewSearchPath;
                      EraseFileList;
                      ScanForFiles(CurrentPath,SearchPath);
                      SortFileList;
                      StartNum := 1;
                      OldStartNum := 1;
                      i := 1;
                      WriteFileList(StartNum);
                      LightName(StartNum,i,true);
                      WriteInfo(i);
                    end;
        Return    : if FileList[i]^.Attr = Directory then
                    begin
                      if FileList[i]^.Name = '..' then
                      begin
                        j := Length(CurrentPath);
                        repeat
                          Dec(j);
                        until CurrentPath[j]='\';
                        CurrentPath := Copy(CurrentPath,1,j);
                      end
                      else
                        CurrentPath := CurrentPath + FileList[i]^.Name+'\';
                      EraseFileList;
                      ScanForFiles(CurrentPath,SearchPath);
                      SortFileList;
                      StartNum := 1;
                      OldStartNum := 1;
                      i := 1;
                      WriteFileList(StartNum);
                      LightName(StartNum,i,true);
                      WriteInfo(i);
                      Key := NullKey;
                    end;
      end;
      if (i-StartNum < 0) and (StartNum>10) then Dec(StartNum,10);
      if (i-StartNum >= 30) then Inc(StartNum,10);
      if StartNum<>OldStartNum then
      begin
        WriteFileList(StartNum);
        OldStartNum := StartNum;
      end;
      LightName(StartNum,i,true);
      WriteInfo(i);
    until Key in [Escape,Return];
    if Key=Return then Filename := CurrentPath+FileList[i]^.Name;
  end; { SelectFile }

begin
  ImSize := 2*19*46;
  GetMem(Scr,ImSize);
  StoreToMem(OpenRow,OpenCol,19,46,Scr^);
  Size := SizeOf(FileType);
  ScanForFiles(CurrentPath,SearchPath);
  SortFileList;
  DrawBackground;
  SelectFile;
  EraseFileList;
  StoreToScr(OpenRow,OpenCol,19,46,Scr^);
  FreeMem(Scr,ImSize);
end; { OpenFile }

end. { UNCommon }
