unit UColors;
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ File    : UCOLORS.PAS                                                    ³}
{³ Author  : Harald Thunem                                                  ³}
{³ Purpose : VGA color palette routines                                     ³}
{³ Updated : October 28 1993                                                ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}

{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ These routines uses BIOS interrupt $10, function $10 to manipulate the   ³}
{³ EGA/VGA palette registers                                                ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure GetTEXTPalette(PaletteReg: byte;  var ColorNum: byte);         ³}
{³ Purpose:  Gets the color number from one of the 16 palette registers     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure SetTEXTPalette(PaletteReg,ColorNum: byte);                     ³}
{³ Purpose:  Sets the color number in one of the 16 palette registers       ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure GetDACRegister(ColorNum: byte;  var RedValue,GreenValue,       ³}
{³                                               BlueValue: byte);          ³}
{³ Purpose:  Gets the Red, Green and Blue byte values from DAC register for ³}
{³           ColorNum                                                       ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure SetDACRegister(ColorNum,RedValue,GreenValue, BlueValue: byte); ³}
{³ Purpose:  Sets the Red, Green and Blue byte values in DAC register for   ³}
{³           ColorNum                                                       ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure GetColorList;                                                  ³}
{³ Purpose:  Gets the complete RGB palette from the DAC registers and       ³}
{³           returns it in the array ColorList                              ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure SetColorList;                                                  ³}
{³ Purpose:  Sets the complete RGB palette in the DAC registers using the   ³}
{³           array ColorList                                                ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  ReadDACFile(Filename: string): boolean;                        ³}
{³ Purpose:  Reads the RGB palette info from the file Filename and puts the ³}
{³           values in the array ColorList. Returns false in file read was  ³}
{³           unsuccessful. Does not call SetColorList!                      ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure WriteDACFile(Filename: string);                                ³}
{³ Purpose:  Writes the RGB palette info in the array ColorList to the file ³}
{³           Filename.                                                      ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ Compiler directives ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
{$O+   Overlay code generation                                               }
{$F+   Force far calls                                                       }
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
interface
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}

const
  CList    : array[0..15] of byte = ($00,$01,$02,$03,$04,$05,$14,$07,
                                     $38,$39,$3A,$3B,$3C,$3D,$3E,$3F);


type
  TColor   = record
               R,G,B : byte;
             end;


var
  ColorList: array[0..15] of TColor;


procedure GetTEXTPalette(PaletteReg: byte;  var ColorNum: byte);
procedure SetTEXTPalette(PaletteReg,ColorNum: byte);
procedure GetDACRegister(ColorNum: byte;  var RedValue,GreenValue,BlueValue: byte);
procedure SetDACRegister(ColorNum,RedValue,GreenValue,BlueValue: byte);
procedure GetColorList;
procedure SetColorList;
function ReadDACFile(Filename: string): boolean;
procedure WriteDACFile(Filename: string);


{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
implementation
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}

uses
  Dos; { TP DOS routines }

var
  Regs : registers;
  CFile: file of TColor;


procedure GetTEXTPalette(PaletteReg: byte;  var ColorNum: byte);
begin
  FillChar(Regs,SizeOf(Regs),$00);
  Regs.AH := $10;
  Regs.AL := $07;
  Regs.BL := PaletteReg;
  Intr($10,Regs);
  ColorNum := Regs.BH;
end; { GetTEXTPalette }


procedure SetTEXTPalette(PaletteReg,ColorNum: byte);
begin
  FillChar(Regs,SizeOf(Regs),$00);
  Regs.AH := $10;
  Regs.AL := $00;
  Regs.BL := PaletteReg;
  Regs.BH := ColorNum;
  Intr($10,Regs);
end; { SetTEXTPalette }


procedure GetDACRegister(ColorNum: byte;  var RedValue,GreenValue,BlueValue: byte);
begin
  FillChar(Regs,SizeOf(Regs),$00);
  Regs.AH := $10;
  Regs.AL := $15;
  Regs.BX := ColorNum;
  Intr($10,Regs);
  RedValue   := Regs.DH;
  GreenValue := Regs.CH;
  BlueValue  := Regs.CL;
end; { GetDACRegister }


procedure SetDACRegister(ColorNum,RedValue,GreenValue,BlueValue: byte);
begin
  FillChar(Regs,SizeOf(Regs),$00);
  Regs.AH := $10;
  Regs.AL := $10;
  Regs.BX := ColorNum;
  Regs.DH := RedValue;
  Regs.CH := GreenValue;
  Regs.CL := BlueValue;
  Intr($10,Regs);
end; { SetDACRegister }


procedure GetColorList;
var
  i: byte;
begin
  for i := 0 to 15 do
  with ColorList[i] do
    GetDACRegister(CList[i],R,G,B);
end; { GetColorList }


procedure SetColorList;
var
  i: byte;
begin
  for i := 0 to 15 do
  with ColorList[i] do
    SetDACRegister(CList[i],R,G,B);
end; { SetColorList }


function ReadDACFile(Filename: string): boolean;
var
  i: byte;
begin
  {$I-}
  Assign(CFile,Filename);
  ReSet(CFile);
  {$I+}
  if IOResult=0 then
  begin
    for i := 0 to 15 do
      Read(CFile,ColorList[i]);
    Close(CFile);
    ReadDACFile := true;
  end
  else ReadDACFile := false;
end; { ReadDACFile }


procedure WriteDACFile(Filename: string);
var
  i: byte;
begin
  Assign(CFile,Filename);
  ReWrite(CFile);
  for i := 0 to 15 do
    Write(CFile,ColorList[i]);
  Close(CFile);
end; { WriteDACFile }

end. { UColors }
