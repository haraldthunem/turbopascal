unit UMatrix;
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ File    : UMATRIX.PAS                                                    ³}
{³ Author  : Harald Thunem                                                  ³}
{³ Purpose : Matrix routines, DOS version                                   ³}
{³ Updated : May 4 1994                                                     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}

{ÄÄÄÄÄÄÄÄ TMatrix object ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ constructor TMatrix.Init( R,C: word);                                    ³}
{³ Purpose:  Allocates space and initializes the matrix with size R x C     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ destructor TMatrix.Done;                                                 ³}
{³ Purpose:  Frees the memory space used by the matrix.                     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  TMatrix.GetData( R,C: word): real;                             ³}
{³ Purpose:  Retrieves the data at position R(row) and C(column) in the     ³}
{³           matrix.                                                        ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.PutData( R,C: word;  Value: real);                     ³}
{³ Purpose:  Puts the data in Value at position R(row) and C(column) in the ³}
{³           matrix.                                                        ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.Write;                                                 ³}
{³ Purpose:  Uses the TP Write routine to write the matrix.                 ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.RandomMatrix( Min, Max: real);                         ³}
{³ Purpose:  Fills the matrix with random values between Min and Max.       ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  TMatrix.DetMatrix: real;                                       ³}
{³ Purpose:  Calculates the determinant of the matrix.                      ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  TMatrix.InvMatrix: boolean;                                    ³}
{³ Purpose:  Inverts the matrix M.                                          ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  TMatrix.TransMatrix: boolean;                                  ³}
{³ Purpose:  Transposes the matrix M.                                       ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.AddScalar( Scalar: real);                              ³}
{³ Purpose:  Adds a scalar value to each element of the matrix.             ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.MultScalar( Scalar: real);                             ³}
{³ Purpose:  Multiplies each element of the matrix with a scalar value.     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.SaveMatrix( Filename: string);                         ³}
{³ Purpose:  Saves the matrix to a matrix file.                             ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure TMatrix.SaveAsciiMatrix( Filename: string);                    ³}
{³ Purpose:  Saves the matrix to an ASCII file.                             ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}

{ÄÄÄÄÄÄÄÄ Other routines ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  AddMatrix( M1,M2: PMatrix): PMatrix;                           ³}
{³ Purpose:  Adds the two matrices M1 and M2 and puts result in AddMatrix.  ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  SubMatrix( M1,M2: PMatrix): PMatrix;                           ³}
{³ Purpose:  Subtracts the matrix M2 from M1 and puts result in SubMatrix.  ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  CopyMatrix( M: PMatrix): PMatrix;                              ³}
{³ Purpose:  Copies the matrix M and puts result in CopyMatrix.             ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  MultMatrix( M1,M2: PMatrix): PMatrix;                          ³}
{³ Purpose:  Multiplies the two matrices M1 and M2 and puts result in       ³}
{³           MultMatrix.                                                    ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  CopyTransMatrix( M: PMatrix): PMatrix;                         ³}
{³ Purpose:  Copies the matrix M and puts transposed result in              ³}
{³           CopyTransMatrix.                                               ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  LoadMatrix( Filename: string): PMatrix;                        ³}
{³ Purpose:  Loads a matrix from a matrix file and puts it in LoadMatrix.   ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  LoadAsciiMatrix( Filename: string): PMatrix;                   ³}
{³ Purpose:  Loads a matrix from an ASCII file and puts it in               ³}
{³           LoadAsciiMatrix.                                               ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  MeanVector( M: PMatrix): PMatrix;                              ³}
{³ Purpose:  Returns a vector containing the mean values of each column     ³}
{³           of matrix M.                                                   ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  CovMatrix( M: PMatrix): PMatrix;                               ³}
{³ Purpose:  Returns the covariance matrix of M.                            ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ function  CorrMatrix( M: PMatrix): PMatrix;                              ³}
{³ Purpose:  Returns the correlation matrix of M.                           ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure AddMatrix2( M1,M2,Res: PMatrix);                               ³}
{³ Purpose:  Same as AddMatrix, but puts result in existing matrix Res.     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure SubMatrix2( M1,M2,Res: PMatrix);                               ³}
{³ Purpose:  Same as SubMatrix, but puts result in existing matrix Res.     ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure CopyMatrix2( M,Res: PMatrix);                                  ³}
{³ Purpose:  Same as CopyMatrix, but puts result in existing matrix Res.    ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure MultMatrix2( M1,M2,Res: PMatrix;                               ³}
{³ Purpose:  Same as MultMatrix, but puts result in existing matrix Res.    ³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}
{ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿}
{³ procedure CopyTransMatrix2( M,Res: PMatrix);                             ³}
{³ Purpose:  Same as CopyTransMatrix, but puts result in existing matrix Res³}
{ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ}

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ Compiler directives ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
{$O+   Overlay code generation                                               }
{$F+   Force far calls                                                       }
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
interface
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}

type
  PMatrix= ^TMatrix;
  TMatrix= object
    Rows,
    Cols : word;
  private
    Data : pointer;
  public
    constructor Init( R, C: word);
    destructor  Done;
    function    GetData( R,C: word): real; virtual;
    procedure   PutData( R,C: word;  Value: real); virtual;
    procedure   Write; virtual;
    procedure   RandomMatrix( Min, Max: real); virtual;
    function    DetMatrix: real; virtual;
    function    InvMatrix: boolean; virtual;
    function    TransMatrix: boolean; virtual;
    procedure   AddScalar( Scalar: real); virtual;
    procedure   MultScalar( Scalar: real); virtual;
    procedure   SaveMatrix( Filename: string); virtual;
    procedure   SaveAsciiMatrix( Filename: string); virtual;
  end;

  TFile = file of real;

var
  Width, Decimals: byte;


function AddMatrix( M1,M2: PMatrix): PMatrix;
function SubMatrix( M1,M2: PMatrix): PMatrix;
function CopyMatrix( M: PMatrix): PMatrix;
function MultMatrix( M1,M2: PMatrix): PMatrix;
function CopyTransMatrix( M: PMatrix): PMatrix;
function LoadMatrix( Filename: string): PMatrix;
function LoadAsciiMatrix( Filename: string): PMatrix;
function MeanVector( M: PMatrix): PMatrix;
function CovMatrix( M: PMatrix): PMatrix;
function CorrMatrix( M: PMatrix): PMatrix;

procedure AddMatrix2( M1,M2,Res: PMatrix);
procedure SubMatrix2( M1,M2,Res: PMatrix);
procedure CopyMatrix2( M,Res: PMatrix);
procedure MultMatrix2( M1,M2,Res: PMatrix);
procedure CopyTransMatrix2( M,Res: PMatrix);

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
implementation
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}


{ÄÄÄÄÄÄÄÄ TMatrix object routine declarations ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
constructor TMatrix.Init( R,C: word);
var
  i,j: word;
begin
  Rows := R;
  Cols := C;
  if MaxAvail < (Rows*Cols*SizeOf( Real)) then
  begin
    Data := NIL;
    Exit;
  end;
  GetMem( Data, Rows*Cols*SizeOf(Real));
  for i := 1 to Rows do
  for j := 1 to Cols do
    PutData( i, j, 0.0);
end; { TMatrix.InitMatrix }

destructor TMatrix.Done;
begin
  if Data <> NIL then
    FreeMem( Data, Rows*Cols*SizeOf(Real));
end; { TMatrix.Done }

function TMatrix.GetData( R,C: word): real;
var
  Segment,
  Offset: word;
  Adr   : ^Real;
begin
  if (R>Rows) or (C>Cols) then Exit;
  Segment := Seg(Data^);
  Offset := Ofs(Data^) + ((R-1)*Cols + (C-1))*SizeOf( Real);
  Adr := Ptr( Segment, Offset);
  GetData := Adr^;
end; { TMatrix.GetData }

procedure TMatrix.PutData( R,C: word;  Value: real);
var
  Segment,
  Offset: word;
  Adr   : ^Real;
begin
  if (R>Rows) or (C>Cols) then Exit;
  Segment := Seg(Data^);
  Offset := Ofs(Data^) + ((R-1)*Cols + (C-1))*SizeOf( Real);
  Adr := Ptr( Segment, Offset);
  Adr^ := Value;
end; { TMatrix.PutData }

procedure TMatrix.Write;
const
  MinValue = 1E-10;
var
  i,j: word;
  r  : real;
begin
  for i := 1 to Rows do
  begin
    for j := 1 to Cols do
    begin
      r := GetData( i, j);
      if Abs( r) < MinValue then r := 0.0;
      System.Write( r: Width: Decimals);
    end;
    WriteLn;
  end;
end; { TMatrix.List }

procedure TMatrix.RandomMatrix( Min, Max: real);
var
  Delta: real;
  i,r,c: word;
begin
  Delta := Max-Min;
  for r := 1 to Rows do
  for c := 1 to Cols do
    PutData( r, c, Min+Delta*Random);
end; { TMatrix.RandomMatrix }

function TMatrix.DetMatrix: real;
const
  MinValue=1E-10;
var
  Tmp  : PMatrix;
  i,j,k: word;
  Factor,
  f,v,v1,v2  : real;
begin
  if (Rows <> Cols) then
  begin
    DetMatrix := 0.0;
    Exit;
  end;
  Tmp := New( PMatrix, Init( Rows, Cols));
  for i := 1 to Tmp^.Rows do
  for j := 1 to Tmp^.Cols do
    Tmp^.PutData( i, j, GetData( i, j));
  for i := 1 to Tmp^.Rows-1 do
  for j := i+1 to Tmp^.Rows do
  begin
    v1 := GetData( j, i);
    v2 := GetData( i, i);
    if Abs( v2) > MinValue then
    begin
      Factor :=  - v1 / v2;
      for k := i to Tmp^.Cols do
      begin
        v := Tmp^.GetData( i, k) * Factor + Tmp^.GetData( j, k);
        Tmp^.PutData( j, k, v);
      end;
    end;
  end;
  v := 1.0;
  for i := 1 to Tmp^.Rows do
    v := v * Tmp^.GetData( i, i);
  Dispose( Tmp);
  DetMatrix := v;
end; { TMatrix.DetMatrix }

function TMatrix.InvMatrix: boolean;
const
  MinValue = 1E-10;
var
  i,j,k,l    : word;
  v1,v2,
  Factor,Tmp : real;
  TmpMatrix1,
  TmpMatrix2 : PMatrix;
begin
  InvMatrix := false;
  if (Rows <> Cols) then Exit;
  TmpMatrix1 := New( PMatrix, Init( Rows, Cols));
  TmpMatrix2 := New( PMatrix, Init( Rows, Cols));
  if (TmpMatrix1=nil) or (TmpMatrix2=nil) then Exit;
  for i := 1 to Rows do
  begin
    TmpMatrix2^.PutData( i, i, 1.0);
    for j := 1 to Cols do
      TmpMatrix1^.PutData( i, j, GetData( i, j));
  end;
  for j := 1 to Cols do
  begin
    i := j;
    while Abs( TmpMatrix1^.GetData( i, j)) < MinValue do
    begin
      if i = Rows then
      begin
        Dispose( TmpMatrix2, Done);
        Dispose( TmpMatrix1, Done);
        Exit;
      end;
      Inc( i);
    end;
    for k := 1 to Cols do
    begin
      Tmp := TmpMatrix1^.GetData( i, k);
      TmpMatrix1^.PutData( i, k, TmpMatrix1^.GetData( j, k));
      TmpMatrix1^.PutData( j, k, Tmp);
      Tmp := TmpMatrix2^.GetData( i, k);
      TmpMatrix2^.PutData( i, k, TmpMatrix2^.GetData( j, k));
      TmpMatrix2^.PutData( j, k, Tmp);
    end;
    factor := 1.0 / TmpMatrix1^.GetData( j, j);
    for k := 1 to Cols do
    begin
      TmpMatrix1^.PutData( j, k, Factor * TmpMatrix1^.Getdata( j, k));
      TmpMatrix2^.PutData( j, k, Factor * TmpMatrix2^.Getdata( j, k));
    end;
    for l := 1 to Rows do
    if l <> j then
    begin
      Factor := -TmpMatrix1^.GetData( l, j);
      for k := 1 to TmpMatrix2^.Cols do
      begin
        v1 := TmpMatrix1^.GetData( l, k);
        v2 := TmpMatrix1^.GetData( j, k);
        TmpMatrix1^.PutData( l, k, v1+Factor*v2);
        v1 := TmpMatrix2^.GetData( l, k);
        v2 := TmpMatrix2^.GetData( j, k);
        TmpMatrix2^.PutData( l, k, v1+Factor*v2);
      end;
    end;
  end;
  Dispose( TmpMatrix1, Done);
  for i := 1 to Rows do
  for j := 1 to Cols do
    PutData( i, j, TmpMatrix2^.GetData( i, j));
  Dispose( TmpMatrix2, Done);
  InvMatrix := true;
end; { TMatrix.InvMatrix }

function TMatrix.TransMatrix: boolean;
var
  i,j,k: word;
  Tmp  : PMatrix;
begin
  TransMatrix := false;
  Tmp := New( PMatrix, Init( Cols, Rows));
  if Tmp=nil then Exit;
  TransMatrix := true;
  for i := 1 to Rows do
  for j := 1 to Cols do
    Tmp^.PutData( j, i, GetData( i, j));

  k := Cols;
  Cols := Rows;
  Rows := k;
  for i := 1 to Rows do
  for j := 1 to Cols do
    PutData( i, j, Tmp^.GetData( i, j));
  Dispose( Tmp, Done);
end; { TMatrix.TransMatrix }

procedure TMatrix.AddScalar( Scalar: real);
var
  i,j: word;
begin
  for i := 1 to Rows do
  for j := 1 to Cols do
    PutData( i, j, Scalar+GetData( i, j));
end; { TMatrix.AddScalar }

procedure TMatrix.MultScalar( Scalar: real);
var
  i,j: word;
begin
  for i := 1 to Rows do
  for j := 1 to Cols do
    PutData( i, j, Scalar*GetData( i, j));
end; { TMatrix.MultScalar }

procedure TMatrix.SaveMatrix( Filename: string);
var
  i,j: word;
  f  : TFile;
  r  : real;
begin
  Assign( f, Filename);
  Rewrite( f);
  r := Rows;
  System.Write( f, r);
  r := Cols;
  System.Write( f, r);
  for i := 1 to Rows do
  for j := 1 to Cols do
  begin
    r := GetData( i, j);
    System.Write( f, r);
  end;
  Close( f);
end; { TMatrix.SaveMatrix }

procedure TMatrix.SaveAsciiMatrix( Filename: string);
var
  i,j: word;
  f  : text;
  r  : real;
begin
  Assign( f, Filename);
  Rewrite( f);
  WriteLn( f, Rows);
  WriteLn( f, Cols);
  for i := 1 to Rows do
  begin
    for j := 1 to Cols do
      System.Write( f, GetData( i, j):(5+Decimals):Decimals);
    WriteLn( f);
  end;
  Close( f);
end; { TMatrix.SaveAsciiMatrix }


{ÄÄÄÄÄÄÄÄ Other matrix routines ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
function AddMatrix( M1,M2: PMatrix): PMatrix;
var
  R,C,
  i,j: word;
  v1,v2: real;
  TmpMatrix: PMatrix;
begin
  if (M1=nil) or (M2=nil) then Exit;
  if M1^.Rows > M2^.Rows then R := M1^.Rows else R := M2^.Rows;
  if M1^.Cols > M2^.Cols then C := M1^.Cols else C := M2^.Cols;
  TmpMatrix := New( PMatrix, Init( R, C));
  if TmpMatrix=nil then Exit;
  for i := 1 to R do
  for j := 1 to C do
  begin
    if (i <= M1^.Rows) and (j <= M1^.Cols) then v1 := M1^.GetData( i, j) else v1:=0.0;
    if (i <= M2^.Rows) and (j <= M2^.Cols) then v2 := M2^.GetData( i, j) else v2:=0.0;
    TmpMatrix^.PutData( i, j, v1+v2);
  end;
  AddMatrix := TmpMatrix;
end; { AddMatrix }

function SubMatrix( M1,M2: PMatrix): PMatrix;
var
  R,C,
  i,j: word;
  v1,v2: real;
  TmpMatrix: PMatrix;
begin
  if (M1=nil) or (M2=nil) then Exit;
  if M1^.Rows > M2^.Rows then R := M1^.Rows else R := M2^.Rows;
  if M1^.Cols > M2^.Cols then C := M1^.Cols else C := M2^.Cols;
  TmpMatrix := New( PMatrix, Init( R, C));
  if TmpMatrix=nil then Exit;
  for i := 1 to R do
  for j := 1 to C do
  begin
    if (i <= M1^.Rows) and (j <= M1^.Cols) then v1 := M1^.GetData( i, j) else v1:=0.0;
    if (i <= M2^.Rows) and (j <= M2^.Cols) then v2 := M2^.GetData( i, j) else v2:=0.0;
    TmpMatrix^.PutData( i, j, v1-v2);
  end;
  SubMatrix := TmpMatrix;
end; { SubMatrix }

function CopyMatrix( M: PMatrix): PMatrix;
var
  i,j: word;
  TmpMatrix: PMatrix;
begin
  TmpMatrix := New( PMatrix, Init( M^.Rows, M^.Cols));
  if TmpMatrix=nil then Exit;
  for i := 1 to M^.Rows do
  for j := 1 to M^.Cols do
    TmpMatrix^.PutData( i, j, M^.GetData( i, j));
  CopyMatrix := TmpMatrix;
end; { CopyMatrix }

function MultMatrix( M1,M2: PMatrix): PMatrix;
var
  i,j,k: word;
  v1,v2,Sum: real;
  TmpMatrix: PMatrix;
begin
  if (M1=nil) or (M2=nil) or (M1^.Cols <> M2^.Rows) then Exit;
  TmpMatrix := New( PMatrix, Init( M1^.Rows, M2^.Cols));
  if TmpMatrix=nil then Exit;
  for i := 1 to TmpMatrix^.Rows do
  for j := 1 to TmpMatrix^.Cols do
  begin
    Sum := 0.0;
    for k := 1 to M1^.Cols do
    begin
      v1  := M1^.GetData( i, k);
      v2  := M2^.GetData( k, j);
      Sum := Sum + v1*v2;
    end;
    TmpMatrix^.PutData( i, j, Sum);
  end;
  MultMatrix := TmpMatrix;
end; { MultMatrix }

function CopyTransMatrix( M: PMatrix): PMatrix;
var
  i,j: word;
  TmpMatrix: PMatrix;
begin
  TmpMatrix := CopyMatrix( M);
  TmpMatrix^.TransMatrix;
  CopyTransMatrix := TmpMatrix;
end; { CopyTransMatrix }

function LoadMatrix( Filename: string): PMatrix;
var
  i,j: word;
  f  : TFile;
  r  : real;
  TmpMatrix: PMatrix;
begin
  {$I-}
  Assign( f, Filename);
  Reset( f);
  {$I+}
  if (IOResult<>0) or (Filename='') then Exit;
  Read( f, r);
  i := Trunc( r);
  Read( f, r);
  j := Trunc( r);
  TmpMatrix := New( PMatrix, Init( i, j));
  if TmpMatrix=nil then Exit;
  for i := 1 to TmpMatrix^.Rows do
  for j := 1 to TmpMatrix^.Cols do
  begin
    Read( f, r);
    TmpMatrix^.PutData( i, j, r);
  end;
  Close( f);
  LoadMatrix := TmpMatrix;
end; { LoadMatrix }

function LoadAsciiMatrix( Filename: string): PMatrix;
var
  i,j: word;
  f  : text;
  r  : real;
  TmpMatrix: PMatrix;
begin
  {$I-}
  Assign( f, Filename);
  Reset( f);
  {$I+}
  if (IOResult<>0) or (Filename='') then Exit;
  ReadLn( f, i);
  ReadLn( f, j);
  TmpMatrix := New( PMatrix, Init( i, j));
  if TmpMatrix=nil then Exit;
  for i := 1 to TmpMatrix^.Rows do
  begin
    for j := 1 to TmpMatrix^.Cols do
    begin
      Read( f, r);
      TmpMatrix^.PutData( i, j, r);
    end;
    ReadLn( f);
  end;
  Close( f);
  LoadASCIIMatrix := TmpMatrix;
end; { LoadAsciiMatrix }

function MeanVector( M: PMatrix): PMatrix;
var
  i,j: integer;
  f,r: real;
  TmpMatrix: PMatrix;
begin
  TmpMatrix := New( PMatrix, Init( M^.Cols, 1));
  if TmpMatrix=nil then Exit;
  f := 1.0 / M^.Rows;
  for j := 1 to M^.Cols do
  begin
    r := 0.0;
    for i := 1 to M^.Rows do
      r := r + M^.GetData( i, j);
    TmpMatrix^.PutData( j, 1, f*r);
  end;
  MeanVector := TmpMatrix;
end;

function CovMatrix( M: PMatrix): PMatrix;
var
  Mean,
  TmpMatrix: PMatrix;
  i,j,r: integer;
  Tmp,f,t1,t2: real;
begin
  if M^.Rows<2 then Exit;
  Mean := MeanVector( M);
  Mean^.Write;
  if Mean=nil then Exit;
  TmpMatrix := New( PMatrix, Init( M^.Cols, M^.Cols));
  if TmpMatrix=nil then Exit;
  f := 1.0 / (M^.Rows-1);
  for i := 1 to M^.Cols do
  for j := 1 to M^.Cols do
  begin
    Tmp := 0.0;
    for r := 1 to M^.Rows do
    begin
      t1 := M^.GetData( r, i)-Mean^.GetData( i, 1);
      t2 := M^.GetData( r, j)-Mean^.GetData( j, 1);
      Tmp := Tmp + (t1*t2);
    end;
    TmpMatrix^.PutData( i, j, f*Tmp);
  end;
  Dispose( Mean, Done);
  CovMatrix := TmpMatrix;
end;

function CorrMatrix( M: PMatrix): PMatrix;
var
  i,j : integer;
  Cov,
  TmpMatrix : PMatrix;
  Tmp1,
  Tmp2,
  Tmp3: real;
begin
  Cov := CovMatrix( M);
  TmpMatrix := New( PMatrix, Init( Cov^.Rows, Cov^.Cols));
  if TmpMatrix=nil then Exit;
  for i := 1 to Cov^.Rows do
  for j := 1 to Cov^.Cols do
  begin
    Tmp1 := Cov^.GetData( i, j);
    Tmp2 := Cov^.GetData( i, i) * Cov^.GetData( j, j);
    Tmp3 := Tmp1 / Sqrt( Tmp2);
    TmpMatrix^.PutData( i, j, Tmp3);
  end;
  Dispose( Cov, Done);
  CorrMatrix := TmpMatrix;
end;

procedure AddMatrix2( M1,M2,Res: PMatrix);
var
  R,C,
  i,j: word;
  v1,v2: real;
begin
  if (M1=nil) or (M2=nil) or (Res=nil) then Exit;
  if M1^.Rows > M2^.Rows then R := M1^.Rows else R := M2^.Rows;
  if M1^.Cols > M2^.Cols then C := M1^.Cols else C := M2^.Cols;
  for i := 1 to R do
  for j := 1 to C do
  begin
    if (i <= M1^.Rows) and (j <= M1^.Cols) then v1 := M1^.GetData( i, j) else v1:=0.0;
    if (i <= M2^.Rows) and (j <= M2^.Cols) then v2 := M2^.GetData( i, j) else v2:=0.0;
    Res^.PutData( i, j, v1+v2);
  end;
end; { AddMatrix2 }

procedure SubMatrix2( M1,M2,Res: PMatrix);
var
  R,C,
  i,j: word;
  v1,v2: real;
begin
  if (M1=nil) or (M2=nil) or (Res=nil) then Exit;
  if M1^.Rows > M2^.Rows then R := M1^.Rows else R := M2^.Rows;
  if M1^.Cols > M2^.Cols then C := M1^.Cols else C := M2^.Cols;
  for i := 1 to R do
  for j := 1 to C do
  begin
    if (i <= M1^.Rows) and (j <= M1^.Cols) then v1 := M1^.GetData( i, j) else v1:=0.0;
    if (i <= M2^.Rows) and (j <= M2^.Cols) then v2 := M2^.GetData( i, j) else v2:=0.0;
    Res^.PutData( i, j, v1-v2);
  end;
end; { SubMatrix2 }

procedure CopyMatrix2( M,Res: PMatrix);
var
  i,j: word;
begin
  if (M=nil) or (Res=nil) then Exit;
  if (M^.Rows <> Res^.Rows) or (M^.Cols <> Res^.Cols) then Exit;
  for i := 1 to M^.Rows do
  for j := 1 to M^.Cols do
    Res^.PutData( i, j, M^.GetData( i, j));
end; { CopyMatrix2 }

procedure MultMatrix2( M1,M2,Res: PMatrix);
var
  i,j,k: word;
  v1,v2,Sum: real;
begin
  if (M1=nil) or (M2=nil) or (Res=nil) then Exit;
  if (M1^.Cols <> M2^.Rows) or (Res^.Rows <> M1^.Rows) or (Res^.Cols <> M2^.Cols) then Exit;
  for i := 1 to Res^.Rows do
  for j := 1 to Res^.Cols do
  begin
    Sum := 0.0;
    for k := 1 to M1^.Cols do
    begin
      v1  := M1^.GetData( i, k);
      v2  := M2^.GetData( k, j);
      Sum := Sum + v1*v2;
    end;
    Res^.PutData( i, j, Sum);
  end;
end; { MultMatrix2 }

procedure CopyTransMatrix2( M,Res: PMatrix);
var
  i,j: word;
begin
  if (M^.Rows <> Res^.Cols) or (M^.Cols <> Res^.Rows) then Exit;
  for i := 1 to M^.Rows do
  for j := 1 to M^.Cols do
    Res^.PutData( j, i, M^.GetData( i, j));
end;

{ÄÄÄÄÄÄÄÄ Unit Initialization ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
begin
  Width := 7;
  Decimals := 2;
end.
