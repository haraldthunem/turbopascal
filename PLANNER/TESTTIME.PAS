program TestTime;
{$F+}
uses
  WinDos,
  WinCrt;

const
  TimerInt = $08;

var
  OldTimerVec   : pointer;
  Hour, Minute,
  Second, Sec100: word;


procedure STI; Inline( $FB);


procedure CallOldInt( Sub: pointer);
begin
  Inline( $9C/$FF/$5E/$06);
end;


procedure WriteTime( Flags, CS, IP, AX, BX, CX, DX, SI, DI, DS, ES, BP: word);
interrupt;
var
  s1,s2,s3: string;
begin
  CallOldInt( OldTimerVec);
  GetTime( Hour, Minute, Second, Sec100);
  Str( Hour, s1);
  if Hour < 10 then s1 := '0'+s1;
  Str( Minute, s2);
  if Minute < 10 then s2 := '0'+s2;
  Str( Second, s3);
  if Second < 10 then s3 := '0'+s3;
  WriteLn( s1+':'+s2+':'+s3);
  STI;
end;

begin
  GetIntVec( TimerInt, OldTimerVec);
  SetIntVec( TimerInt, @WriteTime);
  repeat
  until KeyPressed;
  SetIntVec( TimerInt, OldTimerVec);
end.