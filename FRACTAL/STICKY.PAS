program Sticky;

uses
  Crt,
  Graph,
  UGr256;

const
  MaxPoints = 5000;

type
  TPoint = record
             X,Y,VX,VY: integer;
           end;

var
  List : array[1..MaxPoints] of TPoint;
  Count: word;
  FractType: (MidPoint,BotLine);


procedure MovePoint( i: word);
var
  Direction: byte;
begin
  with List[i] do
  begin
{    PutPixel( X, Y, Black);}
    Direction := Random( 8);
    case Direction of
      0: Dec( VY);
      1: begin
           Inc( VX);
           Dec( VY);
         end;
      2: Inc( VX);
      3: begin
           Inc( VX);
           Inc( VY);
         end;
      4: Inc( VY);
      5: begin
           Dec( VX);
           Inc( VY);
         end;
      6: Dec( VX);
      7: begin
           Dec( VX);
           Dec( VY);
         end;
    end;
    if VX < -1 then VX := -1;
    if VX > 1 then VX := 1;
    if VY < -1 then VY := -1;
    if VY > 1 then VY := 1;
    X := X + VX;
    Y := Y + VY;
    if X < 20 then X := 20;
    if X > (MaxX-20) then X := MaxX-20;
    if Y < 20 then Y := 20;
    if Y > MaxY then Y := MaxY;
{    PutPixel( X, Y, HTWhite);}
  end;
end;


procedure NewPoint( i: word);
var
  Angle,Radius: real;
begin
  with List[i] do
  begin
    Angle := Random*2*pi;
    Radius := 0.50 * MaxY * Random;
    X := (MaxX div 2) + Round( Radius * Cos( Angle));
    Y := (MaxY div 2) + Round( Radius * Sin( Angle));
    if X < 20 then X := 20;
    if X > (MaxX-20) then X := MaxX-20;
    if Y < 20 then Y := 20;
    if Y > (MaxY-20) then Y := MaxY-20;
    VX := 0;
    VY := 0;
  end;
end;


procedure NewLine( i: word);
begin
  with List[i] do
  begin
    X := 20+Random( (MaxX-40));
    Y := MaxY - Random(MaxY div 4);
  end;
end;


procedure CheckPoint( i: word);
var
  StickyPoint: boolean;
begin
  StickyPoint := false;
  with List[i] do
  begin
    if GetPixel( X, Y-1) <> Black then StickyPoint := true;
    if GetPixel( X+1, Y-1) <> Black then StickyPoint := true;
    if GetPixel( X+1, Y) <> Black then StickyPoint := true;
    if GetPixel( X+1, Y+1) <> Black then StickyPoint := true;
    if GetPixel( X, Y+1) <> Black then StickyPoint := true;
    if GetPixel( X-1, Y+1) <> Black then StickyPoint := true;
    if GetPixel( X-1, Y) <> Black then StickyPoint := true;
    if GetPixel( X-1, Y-1) <> Black then StickyPoint := true;

    if StickyPoint then
    begin
      Inc( Count);
      PutPixel( X, Y, (Count div 50)+1);
      repeat
        case FractType of
          MidPoint: NewPoint( i);
          BotLine : NewLine( i);
        end;
      until GetPixel( X, Y) <> Yellow;
    end;
  end;
end;


procedure MiddlePoint;
var
  i: word;
begin
  Count := 0;
  FractType := MidPoint;
  Initialize;
  DefaultPalette;
  for i := 1 to MaxPoints do
    NewPoint( i);
  PutPixel( MaxX div 2, MaxY div 2, White);
  repeat
    i := Random( MaxPoints) + 1;
    MovePoint( i);
    CheckPoint( i);
  until KeyPressed;
  CloseGraph;
end;


procedure BottomLine;
var
  i    : word;
begin
  Count := 0;
  FractType := BotLine;
  Initialize;
  DefaultPalette;
  for i := 1 to MaxPoints do
    NewLine( i);
  SetColor( White);
  Line( 0, MaxY, MaxX, MaxY);
  repeat
    i := Random( MaxPoints) + 1;
    MovePoint( i);
    CheckPoint( i);
  until KeyPressed;
  CloseGraph;
end;


begin
  Resolution := Vga640x480x256;
  MiddlePoint;
{  BottomLine;}
end.
