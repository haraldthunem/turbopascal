program Saupe;
{$N+}
uses
  Graph,
  UGrInit;

const
  MaxN    = 32;
  MaxLevel= 5;

var
  NRand   : byte;
  GaussAdd,
  GaussFac: real;
  Seed    : longint;
  Plane   : array[0..MaxN,0..MaxN] of single;


function Power(x,n: real): real;
begin
  if x > 0.0 then
    Power := Exp(n*Ln(x))
  else Power := 0.0;
end; { Power }


procedure InitGauss( Seed: longint);
begin
  NRand := 4;
  GaussAdd :=Sqrt( 3*NRand);
  GaussFac := 2*GaussAdd / NRand;
  RandSeed := Seed;
end;


function Gauss: real;
var
  Sum: real;
  i  : byte;
begin
  Sum := 0;
  for i := 1 to NRand do
    Sum := Sum + Random;
  Gauss := GaussFac * Sum - GaussAdd;
end;


procedure InitPlane;
var
  i,j: byte;
begin
  for i := 0 to MaxN do
  for j := 0 to MaxN do
    Plane[i,j] := 0.0;
end;


procedure MidPointFM2D(Sigma,H: single; Addition: boolean; Seed: longint);
var
  Delta: single;
  x,y,D,dD,Stage : integer;

  function f3(Delta,x0,x1,x2: single): single;
  begin
    f3 := 0.33333*(x0+x1+x2) + Delta*Gauss;
  end;

  function f4(Delta,x0,x1,x2,x3: single): single;
  begin
    f4 := 0.25*(x0+x1+x2+x3) + Delta*Gauss;
  end;

begin
  InitGauss( Seed);
  Delta := Sigma;
  Plane[0,0] := Delta*Gauss;
  Plane[0,MaxN] := Delta*Gauss;
  Plane[MaxN,0] := Delta*Gauss;
  Plane[MaxN,MaxN] := Delta*Gauss;
  D := MaxN;
  dD := MaxN div 2;
  for Stage := 1 to MaxLevel do
  begin
    Delta := Delta * Power(0.5, 0.5*H);

    x := dD;
    repeat
      y := dD;
      repeat
        Plane[x,y] := f4(Delta,Plane[x+dD,y+dD],Plane[x+dD,y-dD],Plane[x-dD,y+dD],Plane[x-dD,y-dD]);
        Inc( y, D);
      until y > (MaxN-dD);
      Inc( x, D);
    until x > (MaxN-dD);

    x := 0;
    if Addition then
    repeat
      y := 0;
      repeat
        Plane[x,y] := Plane[x,y] + Delta*Gauss;
        Inc( y, D);
      until y > MaxN;
      Inc( x, D);
    until x > MaxN;

    Delta := Delta * Power(0.5, 0.5*H);

    x := dD;
    repeat
      Plane[x,0]    := f3(Delta,Plane[x+dD,0],Plane[x-dD,0],Plane[x,dD]);
      Plane[x,MaxN] := f3(Delta,Plane[x+dD,MaxN],Plane[x-dD,MaxN],Plane[x,MaxN-dD]);
      Plane[0,x]    := f3(Delta,Plane[0,x+dD],Plane[0,x-dD],Plane[dD,x]);
      Plane[MaxN,x] := f3(Delta,Plane[MaxN,x+dD],Plane[MaxN,x-dD],Plane[MaxN-dD,x]);
      Inc( x, D);
    until x > (MaxN-dD);

    x := dD;
    repeat
      y := D;
      repeat
        Plane[x,y] := f4(Delta,Plane[x,y+dD],Plane[x,y-dD],Plane[x+dD,y],Plane[x-dD,y]);
        Inc( y, D);
      until y > (MaxN-dD);
      Inc( x, D);
    until x > (MaxN-dD);

    x := D;
    repeat
      y := dD;
      repeat
        Plane[x,y] := f4(Delta,Plane[x,y+dD],Plane[x,y-dD],Plane[x+dD,y],Plane[x-dD,y]);
        Inc( y, D);
      until y > (MaxN-dD);
      Inc( x, D);
    until x > (MaxN-dD);

    if Addition then
    begin
      x := 0;
      repeat
        y := 0;
        repeat
          Plane[x,y] := Plane[x,y] + Delta*Gauss;
          Inc( y, D);
        until y > MaxN;
        Inc( x, D);
      until x > MaxN;

      x := dD;
      repeat
        y := dD;
        repeat
          Plane[x,y] := Plane[x,y] + Delta*Gauss;
          Inc( y, D);
        until y > (MaxN-dD);
        Inc( x, D);
      until x > (MaxN-dD);
    end;

    D := D div 2;
    dD := dD div 2;
  end;
end;


procedure DrawPlane;
const
  Delta1=7;
  Delta2=3;
  Delta3=5;
  Height= 10;
var
  i,j: byte;
  Square: array[1..5] of PointType;
begin
  SetColor(White);
  SetFillStyle( 1, DarkGray);
  for i := 0 to MaxN-1 do
  for j := 0 to MaxN-1 do
  begin
    Square[1].X := Delta1*j+Delta2*i;
    Square[1].Y := 100+Delta3*i-Round(Height*Plane[i,j]);

    Square[2].X := Delta1*(j+1)+Delta2*i;
    Square[2].Y := 100+Delta3*i-Round(Height*Plane[i,j+1]);

    Square[3].X := Delta1*(j+1)+Delta2*(i+1);
    Square[3].Y := 100+Delta3*(i+1)-Round(Height*Plane[i+1,j+1]);

    Square[4].X := Delta1*j+Delta2*(i+1);
    Square[4].Y := 100+Delta3*(i+1)-Round(Height*Plane[i+1,j]);

    Square[5].X := Square[1].X;
    Square[5].Y := Square[1].Y;

    FillPoly( 5, Square);
  end;
end;



begin
  InitPlane;
  MidPointFM2D(0.8,0.1,true,155);
  Initialize;
  DrawPlane;
  ReadLn;
  CloseGraph;
end.


{ One-dimensional Brownian motion procedures }

const
  N       = 256;
  MaxLevel= 8;

var
  Level,
  i       : word;
  H,
  X       : array[0..N] of real;
  Delta   : array[1..MaxLevel] of real;


procedure WhiteNoiseBM( Seed: longint);
{ Brownian motion by integrating white noise }
var
  i,MidY: word;
begin
  MidY := MaxY div 2;
  InitGauss( Seed);
  X[0] := 0.0;
  SetColor( Yellow);
  MoveTo( 0, Trunc(MidY*(1-X[0])));

  for i := 1 to N-1 do
  begin
    X[i] := X[i-1] + Gauss / (N-1);
    LineTo( i, Trunc(MidY*(1-X[i])));
  end;
end;


procedure MidPointRecursion(Index0,Index2,Level: word);
var
  Index1: word;
begin
  Index1 := (Index0+Index2) div 2;
  X[Index1] := 0.5*(X[Index0]+X[Index2])+Delta[Level]*Gauss;
  if Level < MaxLevel then
  begin
    MidPointRecursion(Index0,Index1,Level+1);
    MidPointRecursion(Index1,Index2,Level+1);
  end;
end;


procedure MidPointBM(Sigma: real; Seed: longint);
var
  i,MidY: word;
begin
  InitGauss( Seed);
  for i := 0 to N do
    X[i] := 0.0;
  for i := 1 to MaxLevel do
    Delta[i] := Sigma*Power( 0.5, 0.5*(i+1));
  X[0] := 0.0;
  X[N] := Sigma*Gauss;
  MidPointRecursion( 0, N, 1);
  MidY := MaxY div 2;
  SetColor( Yellow);
  MoveTo( 0, Trunc(MidY*(1-X[0])));
  for i := 0 to N do
    LineTo( i, Trunc(MidY*(1-X[i])));
end;


procedure MidPointFM1D(Sigma,H: real; Seed: longint; Color: byte);
var
  i,MidY: word;
begin
  InitGauss( Seed);
  for i := 0 to N do
    X[i] := 0.0;
  for i := 1 to MaxLevel do
    Delta[i] := Sigma*Power( 0.5, i*H) * Sqrt(1-Power(2,2*H-2));
  X[0] := 0.0;
  X[N] := Sigma*Gauss;
  MidPointRecursion( 0, N, 1);
  MidY := MaxY div 2;
  SetColor( Color);
  MoveTo( 0, Trunc(MidY*(1-X[0])));
  for i := 0 to N do
    LineTo( i, Trunc(MidY*(1-X[i])));
end;


procedure AdditionsFM1D(Sigma,H: real; Seed: longint; Color: byte);
var
  Level,i,MidY,SmallD,D: word;
begin
  InitGauss( Seed);
  for i := 0 to N do
    X[i] := 0.0;
  for i := 1 to MaxLevel do
    Delta[i] := Sigma*Power( 0.5, i*H) * Sqrt(0.5) * Sqrt(1-Power(2,2*H-2));
  X[0] := 0.0;
  X[N] := Sigma*Gauss;
  D := N;
  SmallD := D div 2;
  Level := 1;
  while Level <= MaxLevel do
  begin
    i := SmallD;
    repeat
      X[i] := 0.5*(X[i-SmallD] + X[i+SmallD]);
      i := i + D;
    until i > (N-SmallD);
    i := 0;
    repeat
      X[i] := X[i] + Delta[Level]*Gauss;
      i := i + SmallD;
    until i > N;
    D := D div 2;
    SmallD := SmallD div 2;
    Inc( Level);
  end;


  MidY := MaxY div 2;
  SetColor( Color);
  MoveTo( 0, Trunc(MidY*(1-X[0])));
  for i := 0 to N do
    LineTo( i, Trunc(MidY*(1-X[i])));
end;


