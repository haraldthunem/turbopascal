program CreatePalette;
{$N+}
uses  {Crt,}
      Graph,
      UGr256,
      UKeys,
      UStrs;

const PalX           = 20;
      PalY           = 60;

type
      AType          = (Active,InActive,Remove);

var
  MaxColor    : word;     { The maximum color value available }
  PAutoDetect : pointer;
  FileName,s  : string;
  PaletteFile : file of TVGAPalette;
  Color       : integer;
  Component   : byte;


procedure AbsWindow(x1,y1,x2,y2: word;
                    Depth: shortint;
                    HiLight,Normal,LoLight: byte);
begin
  SetFillStyle(1,Normal);
  Bar(x1,y1,x2,y2);
  if Depth = 0 then Exit;
  if Depth > 0 then
    SetColor(HiLight)
  else SetColor(LoLight);
  Line(x1,y1,x2,y1);
  Line(x1,y1,x1,y2);
  if Depth > 0 then
    SetColor(LoLight)
  else SetColor(HiLight);
  Line(x2,y2,x2,y1);
  Line(x2,y2,x1,y2);
end;


procedure RelWindow(x,y,dx,dy: word;
                    Depth: shortint;
                    HiLight,Normal,LoLight: byte);
var x2,y2: word;
begin
  x2 := x + dx;
  y2 := y + dy;
  AbsWindow(x,y,x2,y2,Depth,HiLight,Normal,LoLight);
end;


procedure EditString(X,Y,L,Fg,Bg: word;  var S: string);
var i,SL: byte;
    sx: word;
    Tmp: string;
begin
  Tmp := S;
  SL := Length(S);
  SetFillStyle(1,Bg);
  Bar(X,Y,X+8*L+20,Y+20);
  SetTextStyle(0,0,1);
  SetTextJustify(LeftText,BottomText);
  SetColor(Fg);
  if SL > 0 then
    for i := 1 to SL do
    OutTextXY(X+8*i,Y+15,S[i]);
  sx := X+8*i+9;
  Line(sx,Y+15,sx+6,Y+15);
  repeat
    Inkey(Ch,Key);
    case Key of
      BackSpace: if SL > 0 then
                 begin
                   Bar(sx-9,Y,sx+6,Y+20);
                   Dec(sx,8);
                   Line(sx,Y+15,sx+6,Y+15);
                   S := Copy(S,1,SL-1);
                   Dec(SL);
                 end;
      TextKey,
      NumberKey: if SL < L then
                 begin
                   Bar(sx-1,Y,sx+6,Y+20);
                   Inc(sx,8);
                   Line(sx,Y+15,sx+6,Y+15);
                   Inc(SL);
                   S := S + Ch;
                   OutTextXY(sx-8,Y+15,S[SL]);
                 end;
    end;
  until Key in [Escape,Return];
  if Key=Escape then
    S := Tmp;
end;


procedure GetFilename(var Filename: string);
const X = 230;
      Y = 170;
var   Image: pointer;
      ISize: word;
      OldT : TextSettingsType;
begin
  GetTextSettings(OldT);
  ISize := ImageSize(X,Y,X+180,Y+80);
  GetMem(Image,ISize);
  GetImage(X,Y,X+180,Y+80,Image^);
  RelWindow(X,Y,180,80,1,HTLightGreen,HTGreen,HTDarkGray);
  RelWindow(X+1,Y+1,178,30,1,HTLightGreen,HTGreen,HTDarkGray);
  SetTextStyle(0,0,1);
  SetTextJustify(CenterText,CenterText);
  SetColor(HTDarkGray);
  OutTextXY(X+91,Y+18,'SAVE TO FILE');
  SetColor(HTWhite);
  OutTextXY(X+90,Y+17,'SAVE TO FILE');
  EditString(X+30,Y+45,12,HTYellow,HTDarkGray,Filename);
  PutImage(X,Y,Image^,NormalPut);
  FreeMem(Image,ISize);
  with OldT do
  begin
    SetTextStyle(Font,Direction,CharSize);
    SetTextJustify(Horiz,Vert);
  end;
end;


procedure DrawArrow(x,y: word; ActiveArrow: AType);
var Arrow: array[1..7] of PointType;
    Color,Fill: byte;

  procedure DefArrow(x1,y1: word);
  begin
    Arrow[1].x := x1;    Arrow[1].y := y1;
    Arrow[2].x := x1+6;  Arrow[2].y := y1+9;
    Arrow[3].x := x1+2;  Arrow[3].y := y1+9;
    Arrow[4].x := x1+2;  Arrow[4].y := y1+15;
    Arrow[5].x := x1-2;  Arrow[5].y := y1+15;
    Arrow[6].x := x1-2;  Arrow[6].y := y1+9;
    Arrow[7].x := x1-6;  Arrow[7].y := y1+9;
  end;

begin
  case ActiveArrow of
    Active  : begin
                Color := HTWhite;
                Fill  := HTYellow;
              end;
    InActive: begin
                Color := HTWhite;
                Fill  := HTLightGray;
              end;
  end;
  if ActiveArrow = Remove then
  begin
    SetFillStyle(1,HTLightGray);
    Bar(x-6,y,x+8,y+17);
  end
  else begin
    SetColor(HTDarkGray);
    SetFillStyle(1,HTDarkGray);
    DefArrow(x+2,y+2);
    FillPoly(7,Arrow);

    SetColor(Color);
    SetFillStyle(1,Fill);
    DefArrow(x,y);
    FillPoly(7,Arrow);
  end;
end;


procedure MainBackGround;
var i: byte;
begin
  for i := 0 to 239 do
  begin
    SetColor(i);
    Line(0,2*i,MaxX,2*i);
    Line(0,2*i+1,MaxX,2*i+1);
  end;

  RelWindow(PalX,PalY,600,200,1,HTWhite,HTLightGray,HTDarkGray);
  RelWindow(PalX+5,PalY+30,590,160,-1,HTWhite,HTLightGray,HTDarkGray);
  SetTextJustify(CenterText,CenterText);
  SetTextStyle(0,0,2);
  SetColor(HTDarkGray);
  OutTextXY(PalX+303,PalY+18,'PALETTE EDITOR');
  SetColor(HTWhite);
  OutTextXY(PalX+300,PalY+15,'PALETTE EDITOR');

  RelWindow(PalX+150,PalY+200,300,150,1,HTWhite,HTLightGray,HTDarkGray);
  SetColor(HTLightGray);
  Line(PalX+150,PalY+200,PalX+449,PalY+200);
  RelWindow(PalX+155,PalY+190,290,135,-1,HTWhite,HTLightGray,HTDarkGray);
  SetColor(HTLightGray);
  Line(PalX+155,PalY+190,PalX+444,PalY+190);
  for i := 0 to 79 do
  begin
    SetFillStyle(1,i);
    Bar(PalX+20+7*i,PalY+40,PalX+19+7*(i+1),PalY+70);
  end;
  for i := 80 to 159 do
  begin
    SetFillStyle(1,i);
    Bar(PalX+20+7*(i-80),PalY+90,PalX+19+7*(i-79),PalY+120);
  end;
  for i := 160 to 239 do
  begin
    SetFillStyle(1,i);
    Bar(PalX+20+7*(i-160),PalY+140,PalX+19+7*(i-159),PalY+170);
  end;
  SetColor(HTWhite);
  RelWindow(PalX+275,PalY+211,50,28,1,HTWhite,HTLightGray,HTDarkGray);
  RelWindow(PalX+279,PalY+215,42,20,-1,HTWhite,HTDarkGray,HTBlack);

  RelWindow(PalX+165,PalY+260,50,28,1,HTLightRed,HTRed,HTDarkGray);
  RelWindow(PalX+169,PalY+264,42,20,-1,HTLightRed,HTDarkGray,HTBlack);

  RelWindow(PalX+275,PalY+260,50,28,1,HTLightGreen,HTGreen,HTDarkGray);
  RelWindow(PalX+279,PalY+264,42,20,-1,HTLightGreen,HTDarkGray,HTBlack);

  RelWindow(PalX+385,PalY+260,50,28,1,HTLightBlue,HTBlue,HTDarkGray);
  RelWindow(PalX+389,PalY+264,42,20,-1,HTLightBlue,HTDarkGray,HTBlack);
  SetTextStyle(0,0,1);
  SetColor(HTDarkGray);
  OutTextXY(PalX+301,PalY+201,'COLOR NUMBER');
  OutTextXY(PalX+301,PalY+251,'COLOR COMPONENTS');
  SetColor(HTWhite);
  OutTextXY(PalX+300,PalY+200,'COLOR NUMBER');
  OutTextXY(PalX+300,PalY+250,'COLOR COMPONENTS');

  SetTextJustify(LeftText,TopText);
  SetColor(HTDarkGray);
  OutTextXY(PalX+161,PalY+336,'F2');
  OutTextXY(PalX+181,PalY+336,'-Save');
  OutTextXY(PalX+241,PalY+336,'Esc');
  OutTextXY(PalX+266,PalY+336,'-Quit');
  SetColor(HTYellow);
  OutTextXY(PalX+160,PalY+335,'F2');
  OutTextXY(PalX+240,PalY+335,'Esc');
  SetColor(HTWhite);
  OutTextXY(PalX+180,PalY+335,'-Save');
  OutTextXY(PalX+265,PalY+335,'-Quit');
  SetTextJustify(CenterText,CenterText);
end;


procedure WriteValues(Color: integer);
var s: string;
begin
  SetFillStyle(1,HTDarkGray);
  Bar(PalX+280,PalY+216,PalX+320,PalY+234);
  Bar(PalX+170,PalY+265,PalX+210,PalY+283);
  Bar(PalX+280,PalY+265,PalX+320,PalY+283);
  Bar(PalX+390,PalY+265,PalX+430,PalY+283);
  SetColor(HTYellow);
  OutTextXY(PalX+300,PalY+225,StrL(Color));
  SetColor(HTLightRed);
  OutTextXY(PalX+190,PalY+274,StrL(Palette[Color].r));
  SetColor(HTLightGreen);
  OutTextXY(PalX+300,PalY+274,StrL(Palette[Color].g));
  SetColor(HTLightBlue);
  OutTextXY(PalX+410,PalY+274,StrL(Palette[Color].b));
end;


procedure ReadFromFile(Filename: string);
var i: byte;
begin
  {$i-}
  Assign(PaletteFile,Filename);
  Reset(PaletteFile);
  {$i+}
  if IOResult <> 0 then Exit;
  Read(PaletteFile,Palette);
  Close(PaletteFile);
  SetDACBlock( 0, 256, Palette);
end;


procedure SaveToFile(var Filename: string);
var i: byte;
begin
  GetFilename(Filename);
  if Key=Escape then
  begin
    Key := NullKey;
    Exit;
  end;
  Assign(PaletteFile,Filename);
  Rewrite(PaletteFile);
  Write(PaletteFile,Palette);
  Close(PaletteFile);
  Key := NullKey;
end;


procedure SelectColor(var Color: integer);
var x,y: word;
begin
  Key := NullKey;
  x := PalX + 23 + 7*(Color mod 80);
  y := PalY + 71 + 50*(Color div 80);
  DrawArrow(x,y,Active);
  WriteValues(Color);
  repeat
    Inkey(Ch,Key);
    DrawArrow(x,y,Remove);
    case Key of
      LeftArrow : Dec(Color);
      RightArrow: Inc(Color);
      UpArrow   : Dec(Color,80);
      DownArrow : Inc(Color,80);
      F2        : SaveToFile(Filename);
      AltA      : RandomPalette(Random(64),Random(64),Random(64));
      AltW      : RandomPalette(63,63,63);
      AltD      : RandomPalette(0,0,0);

      AltR      : RandomPalette(63,0,0);
      AltG      : RandomPalette(0,63,0);
      AltB      : RandomPalette(0,0,63);

      AltY      : RandomPalette(63,63,0);
      AltC      : RandomPalette(0,63,63);
      AltM      : RandomPalette(63,0,63);
      AltQ      : DefaultPalette;
      AltS      : SpesialPalette;
    end;
    if Color < 0 then
      Inc(Color,240);
    if Color > 239 then
      Dec(Color,240);
    x := PalX + 23 + 7*(Color mod 80);
    y := PalY + 71 + 50*(Color div 80);
    DrawArrow(x,y,Active);
    WriteValues(Color);
  until Key in [Return,TabKey,Escape];
  DrawArrow(x,y,InActive);
end;


procedure EditColor(Color: integer);
var x,y: word;
    r,g,b: integer;
begin
  Key := NullKey;
  x := PalX + 190 + 110*(Component-1);
  y := PalY + 290;
  DrawArrow(x,y,Active);
  repeat
    Inkey(Ch,Key);
    DrawArrow(x,y,Remove);
    r := Palette[Color].r;
    g := Palette[Color].g;
    b := Palette[Color].b;
    case Key of
      LeftArrow : Dec(Component);
      RightArrow: Inc(Component);
      UpArrow   : case Component of
                    1: Inc(r);
                    2: Inc(g);
                    3: Inc(b);
                  end;
      DownArrow : case Component of
                    1: Dec(r);
                    2: Dec(g);
                    3: Dec(b);
                  end;
    end;
    if Component < 1 then Component := 3;
    if Component > 3 then Component := 1;
    if r<0 then r:=63; if r>63 then r:=0;
    if g<0 then g:=63; if g>63 then g:=0;
    if b<0 then b:=63; if b>63 then b:=0;
    Palette[Color].r:=r;
    Palette[Color].g:=g;
    Palette[Color].b:=b;
    x := PalX + 190 + 110*(Component-1);
    y := PalY + 290;
    DrawArrow(x,y,Active);
    if Key in [UpArrow,DownArrow] then
    begin
      SetPaletteColor(Color,r,g,b);
      SetOneColor( Color, Palette[Color]);
      WriteValues(Color);
    end;
  until Key in [Escape,TabKey,Return];
  DrawArrow(x,y,InActive);
end;


begin
  Filename := 'NONAME00.PAL';
  Resolution := Vga640x480x256;
  Initialize;
{  if ParamCount > 0 then
  begin
    Filename := ParamStr(1);
    ReadFromFile(Filename);
  end
  else}
  DefaultPalette;
  MainBackGround;
  Component := 1;
  Color := 0;
  Key := NullKey;
  repeat
    if Key<>Escape then
      SelectColor(Color);
    if Key<>Escape then
      EditColor(Color);
  until Key=Escape;
  CloseGraph;
end.