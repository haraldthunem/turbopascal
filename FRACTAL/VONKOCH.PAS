program vonKoch;

uses
{  Crt,}
  Graph,
  UGr256,
  UTurtle,
  UKeys,
  UStrs;

const
  MaxAttractors = 3;
  MaxIFS        = 4;

type
  TAttractor = record
    X,Y,Probability: real;
  end;
  TIFS       = record
    h,k,Theta,Omega,r,s,Prob: real;
  end;

var
  Attractor: array[1..MaxAttractors] of TAttractor;
  IFS      : array[1..MaxIFS] of TIFS;
  NumIFS   : byte;


procedure InitIFS;
begin
  NumIFS := 4;
  with IFS[1] do
  begin
    h:=0.0;  k:=0.0;  Theta:=0.0;   Omega:=0.0;   r:=0.00; s:=0.22; Prob:=0.01;
  end;
  with IFS[2] do
  begin
    h:=0.0;  k:=1.0;  Theta:=-2.0;  Omega:=-2.0;  r:=0.85; s:=0.85; Prob:=0.85;
  end;
  with IFS[3] do
  begin
    h:=0.0;  k:=1.0;  Theta:=49.0;  Omega:=49.0;  r:=0.30; s:=0.30; Prob:=0.07;
  end;
  with IFS[4] do
  begin
    h:=0.0;  k:=0.50; Theta:=120.0; Omega:=-50.0; r:=0.30; s:=0.37; Prob:=0.07;
  end;
end;


procedure RenderIFS;
const
  Rad = pi/180;
var
  X,Y  : real;
  Col  : byte;
  Count: integer;

  procedure NewPoint;
  var
    OldX,
    OldY,
    TmpProb,
    SumProb: real;
  begin
    SumProb := 0.0;
    TmpProb := Random;
    Count := 0;
    repeat
      Inc( Count);
      SumProb := SumProb + IFS[Count].Prob;
    until (SumProb >= TmpProb) or (Count>=NumIFS);
    with IFS[Count] do
    begin
      OldX := X;
      OldY := Y;
      X := r*Cos(Rad*Theta)*OldX - s*Sin(Rad*Omega)*OldY + h;
      Y := r*Sin(Rad*Theta)*OldX + s*Cos(Rad*Omega)*OldY + k;
    end;
  end;

begin
  X := Random;
  Y := Random;
  for Col := 1 to 10 do
    NewPoint;
  Key := NullKey;
  repeat
    Col := GetPixel( 500+Round(X * 100), 700-Round(Y * 100));
    if Col >= 239 then Col := 0;
    PutPixel( 500+Round(X * 100), 700-Round(Y * 100), Col+1);
    NewPoint;
    if KeyPressed then
    begin
      InKey( Ch, Key);
      case Key of
        AltA: RandomPalette(Random(64),Random(64),Random(64));
        AltW: RandomPalette(63,63,63);
        AltD: RandomPalette(0,0,0);

        AltR: RandomPalette(40,0,0);
        AltG: RandomPalette(0,40,0);
        AltB: RandomPalette(0,0,40);

        AltY: RandomPalette(40,40,0);
        AltC: RandomPalette(0,40,40);
        AltM: RandomPalette(40,0,40);

        AltQ: DefaultPalette;
        AltS: SpesialPalette;
      end;
    end;
  until Key=Escape;
end;


procedure InitAttractors;
begin
  Attractor[1].X := 100;
  Attractor[1].Y := 701;
  Attractor[1].Probability := 0.25;

  Attractor[2].X := 900;
  Attractor[2].Y := 701;
  Attractor[2].Probability := 0.25;

  Attractor[3].X := 500;
  Attractor[3].Y := 7;
  Attractor[3].Probability := 0.50;

{  Attractor[1].X := 50;
  Attractor[1].Y := 700;
  Attractor[1].Probability := 0.20;

  Attractor[2].X := 1000;
  Attractor[2].Y := 700;
  Attractor[2].Probability := 0.20;

  Attractor[3].X := 1000;
  Attractor[3].Y := 50;
  Attractor[3].Probability := 0.20;

  Attractor[4].X := 50;
  Attractor[4].Y := 50;
  Attractor[4].Probability := 0.40;}
end;


procedure Sierpinski;
var
  X,Y  : real;
  Col,
  i,Count: byte;

  procedure NewPoint;
  var
    TmpProb,
    SumProb: real;
  begin
    SumProb := 0.0;
    TmpProb := Random;
    Count := 0;
    repeat
      Inc( Count);
      SumProb := SumProb + Attractor[Count].Probability;
    until (SumProb >= TmpProb) or (Count>=MaxAttractors);
    X := 0.5*(X + Attractor[Count].X);
    Y := 0.5*(Y + Attractor[Count].Y);
  end;

begin
  DrawPaletteLine;
  InitAttractors;
  X := Random * MaxX;
  Y := Random * MaxY;
  for i := 1 to 10 do
    NewPoint;
  repeat
    NewPoint;
    Col:= GetPixel(Round(X),Round(Y));
    PutPixel(Round(X),Round(Y),Col+1);
  until KeyPressed;
end;


procedure Koch1(Level: byte;  XLength: real);
var
  TmpLength: real;
begin
  TmpLength := XLength/3;
  if Level > 1 then
    Koch1( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 60);
  if Level > 1 then
    Koch1( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 120);
  if Level > 1 then
    Koch1( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 60);
  if Level > 1 then
    Koch1( Level-1, TmpLength)
  else
    Forwd( TmpLength);
end;


procedure Koch2(Level: byte;  XLength: real);
var
  TmpLength: real;
begin
  TmpLength := XLength/3;
  if Level > 1 then
    Koch2( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 90);
  if Level > 1 then
    Koch2( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 90);
  if Level > 1 then
    Koch2( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 90);
  if Level > 1 then
    Koch2( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 90);
  if Level > 1 then
    Koch2( Level-1, TmpLength)
  else
    Forwd( TmpLength);
end;


procedure Koch3(Level: byte;  XLength: real);
var
  TmpLength: real;
begin
  TmpLength := XLength/4;
  if Level > 1 then
    Koch3( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 90);
  if Level > 1 then
    Koch3( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 90);
  if Level > 1 then
    Koch3( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 90);
  if Level > 1 then
  begin
    Koch3( Level-1, TmpLength);
    Koch3( Level-1, TmpLength);
  end
  else
    Forwd( 2*TmpLength);
  TurnLeft( 90);
  if Level > 1 then
    Koch3( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 90);
  if Level > 1 then
    Koch3( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 90);
  if Level > 1 then
    Koch3( Level-1, TmpLength)
  else
    Forwd( TmpLength);
end;


procedure Koch4(Level: byte;  XLength: real);
var
  TmpLength: real;
begin
  TmpLength := XLength/4;
  if Level > 1 then
    Koch4( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 60);
  if Level > 1 then
    Koch4( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 120);
  if Level > 1 then
  begin
    Koch4( Level-1, TmpLength);
    Koch4( Level-1, TmpLength);
  end
  else
    Forwd( 2*TmpLength);
  TurnLeft( 120);
  if Level > 1 then
    Koch4( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 60);
  if Level > 1 then
    Koch4( Level-1, TmpLength)
  else
    Forwd( TmpLength);
end;


procedure KochSierp(Level: byte;  XLength: real);
var
  TmpLength: real;
begin
  TmpLength := 0.50*XLength;
  if Level > 1 then
    KochSierp( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 120);
  if Level > 1 then
    KochSierp( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 120);
  if Level > 1 then
    KochSierp( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnRight( 120);
  if Level > 1 then
    KochSierp( Level-1, TmpLength)
  else
    Forwd( TmpLength);
  TurnLeft( 120);
  if Level > 1 then
    KochSierp( Level-1, TmpLength)
  else
    Forwd( TmpLength);
end;


var
  i: byte;
  c: char;

begin
  Randomize;
  Resolution := Vga1024x768x256;
  Initialize;
  DefaultPalette;
  ClearViewPort;

{  SetColor( HTWhite);
  SetPosition(100,700);
  SetHeading( 0.0);
  KochSierp(6,800);
  c := ReadKey;
  ClearViewPort;}
  Sierpinski;
  ClearViewPort;
  SetColor( White);

{  for i := 1 to 7 do
  begin
    OutTextXY(30,30,'von Koch, Level '+StrL(i));
    SetPosition(300,200);
    SetHeading( 0.0);
    Koch1(i,500);
    SetPosition(800,200);
    SetHeading( 240.0);
    Koch1(i,500);
    SetPosition(550,633);
    SetHeading( 120.0);
    Koch1(i,500);
    c := ReadKey;
    ClearViewPort;
  end;}

{  for i := 1 to 6 do
  begin
    OutTextXY(30,30,'von Koch type 2, Level '+StrL(i));
    SetPosition(300,200);
    SetHeading( 0.0);
    Koch2(i,350);
    SetPosition(650,200);
    SetHeading( 270.0);
    Koch2(i,350);
    SetPosition(650,550);
    SetHeading( 180.0);
    Koch2(i,350);
    SetPosition(300,550);
    SetHeading( 90.0);
    Koch2(i,350);
    c := ReadKey;
    ClearViewPort;
  end;}

{  for i := 1 to 6 do
  begin
    OutTextXY(30,300,StrL(i));
    SetPosition(30,400);
    SetHeading( 0.0);
    Koch3(i,900);
    c := ReadKey;
    ClearViewPort;
  end;}

{  for i := 1 to 6 do
  begin
    OutTextXY(30,300,StrL(i));
    SetPosition(30,400);
    SetHeading( 0.0);
    Koch4(i,900);
    c := ReadKey;
    ClearViewPort;
  end;}

{  InitIFS;
  DrawPaletteLine;
  RenderIFS;}

  CloseGraph;
end.